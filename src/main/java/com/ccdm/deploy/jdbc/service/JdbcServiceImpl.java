package com.ccdm.deploy.jdbc.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.common.constants.SqlConstants;
import com.ccdm.deploy.common.validator.CommonValidator;
import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.infrastructure.config.JdbcConnnectionProvider;
import com.ccdm.deploy.instance.Processor.InstanceJdbcQueries;

@Component
public class JdbcServiceImpl implements JdbcService {

	private static final Logger logger = LoggerFactory.getLogger(JdbcServiceImpl.class);
	@Autowired
	private JdbcConnnectionProvider jdbcConnectionProvider;
	
	@Override
	public Integer getCostDetail(Integer providerId, String instanceType, String costType) throws CCDMException {
		Connection connection = jdbcConnectionProvider.getJdbcConnection();
		PreparedStatement statement = null;
		try {
			statement = (PreparedStatement) connection.prepareStatement(SqlConstants.SELECT_ID_FROM_COSTDETAIL_USING_PROVIDERID_INSTANCETYPE_COSTTYPE);
			statement.setInt(1, providerId);
			statement.setString(2, instanceType);
			statement.setString(3, costType);
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				return resultSet.getInt("ID");
			}
		} catch (SQLException e) {
			logger.error("Sql Exception While Getting CostDetail Id Using JDBC Connection ====>>> ",e.getMessage());
		} finally{
		      //finally block used to close resources
		      try{
		         if(statement!=null)
		        	 connection.close();
		      }catch(SQLException se){
		    	  logger.error("Sql Exception While Getting CostDetail Id Using JDBC Connection ====>>> ", se.getMessage());
		      }// do nothing
		      try{
		         if(connection!=null)
		        	 connection.close();
		      }catch(SQLException se){
		    	  	logger.error("Sql Exception While Getting CostDetail Id Using JDBC Connection ====>>> ",se.getMessage());
		      }//end finally try
		   }
		return null;
	}

	@Override
	public void saveCostReference(Integer instanceInfoId, Integer costDetailsId) throws CCDMException 
	{
		CommonValidator.validateId(instanceInfoId, ErrorCodes.INVALID_INSTANCE_ID);
		if(costDetailsId == null)
		{
			return;
		}
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try 
		{
			connection = jdbcConnectionProvider.getJdbcConnection();
			preparedStatement = (PreparedStatement) connection.prepareStatement(InstanceJdbcQueries.SAVE_COST_DETAIL_REFERENCE);
			preparedStatement.setInt(1, costDetailsId);
			preparedStatement.setInt(2, instanceInfoId);
			preparedStatement.setTimestamp(3, getCurrentTimeStamp());
			int row = preparedStatement.executeUpdate();
			if(row > 0)
			{
				logger.info("Instance Cost Details Saved...............!");
			}
		} 
		catch (SQLException e) {
			logger.error("Sql Exception While Getting CostDetail Id Using JDBC Connection ====>>> ", e.getMessage());
		} 
		finally
		{
		      //finally block used to close resources
		      try
		      {
		         if(preparedStatement!=null)
		        	 connection.close();
		      }
		      catch(SQLException se)
		      {
		    	  logger.error("Sql Exception While Saving CostReference Using JDBC Connection ====>>> ", se.getMessage());
		      }
		      try
		      {
		         if(connection!=null)
		        	 connection.close();
		      }
		      catch(SQLException se)
		      {
		    	  	logger.error("Sql Exception While Saving CostReference Using JDBC Connection ====>>> ", se.getMessage());
		      }//end finally try
		   }
		}

	private static java.sql.Timestamp getCurrentTimeStamp() 
	{
		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());
	}
}
