package com.ccdm.deploy.jdbc.service;

import com.ccdm.deploy.exceptions.CCDMException;

public interface JdbcService {

	Integer getCostDetail(Integer providerId, String instanceType, String costType) throws CCDMException;

	void saveCostReference(Integer instanceInfoId, Integer costDetailsId) throws CCDMException;

}
