package com.ccdm.deploy.webprovider.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.ccdm.deploy.common.enums.ECostType;
import com.ccdm.deploy.elastic.solutionhistory.model.CreateInstanceRequestVo;
import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.jdbc.service.JdbcService;
import com.ccdm.deploy.provider.client.factory.AmazonEC2ClientFactory;
import com.ccdm.deploy.provider.validator.InstanceValidator;
import com.ccdm.deploy.solution.model.ServerCredentialVo;
import com.ccdm.deploy.vo.InstanceInfo;
import com.ccdm.deploy.webServer.provider.task.utils.ProviderUtils;
import com.ccdm.deploy.webprovider.helper.AWSInstanceHelper;
import com.ccdm.deploy.webprovider.helper.WebProviderCommonHelper;
import com.ccdm.deploy.webprovider.response.InstanceCreateResponse;
import com.ccdm.deploy.webprovider.response.InstanceStatusResponse;

@Component("awsProvider")
public class AWSProviderServiceImpl implements IProviderService 
{
	private static final Logger logger = LoggerFactory.getLogger(AWSProviderServiceImpl.class);
	
	@Autowired
	private AmazonEC2ClientFactory amazonEC2ClientFactory;
	@Autowired
	private AWSInstanceHelper awsInstanceHelper;
	@Autowired
	private WebProviderCommonHelper webProviderHelper;
	@Autowired
	private JdbcService jdbcService;
	@Autowired
	private ProviderUtils providerUtils;
	
	@Override
	public InstanceCreateResponse create(CreateInstanceRequestVo request) throws CCDMException 
	{
		try
		{
			InstanceValidator.validateCreateInstaceRequestVo(request);
			InstanceValidator.validateServerCredentialVo(request.getServerCredentialVo());
			
			AmazonEC2 amazonEC2Client = getAmazonEC2(request.getServerCredentialVo());
			
			// 1. Create Instance
			RunInstancesRequest runInstancesRequest = awsInstanceHelper.getCreateRunInstanceRequest(request);
			RunInstancesResult result = amazonEC2Client.runInstances(runInstancesRequest);
			Instance instance = result.getReservation().getInstances().get(0);
			logger.info("Instance '"+request.getInstanceName()+"' Created in AWS ..."); 
			
			// 2.Set Tag Name to Instance
			CreateTagsRequest createTagsRequest = awsInstanceHelper.getCreateTagRequest(instance.getInstanceId(), request.getInstanceName());
			amazonEC2Client.createTags(createTagsRequest);
			logger.info("Tag Name Created for Instance : "+request.getInstanceName()+" in AWS"); 
			
			// 3. Describe Current Instance Status
			providerUtils.sleepThreadByTimeOut("Sleeping 20 sec to Get Latest Instance Status..", 20000);
			DescribeInstancesRequest describeInstancesRequest = awsInstanceHelper.getDescribeInstancesRequest(instance.getInstanceId());
			DescribeInstancesResult describeInstancesResult = amazonEC2Client.describeInstances(describeInstancesRequest);
			instance = awsInstanceHelper.getInstanceFromReservation(describeInstancesResult.getReservations().get(0));
			logger.info("current status of isntance '"+request.getInstanceName()+"' described...");
			
			// 4. Save Instance
			InstanceInfo instanceInfo = awsInstanceHelper.buildInstanceInfo(instance);
			instanceInfo.setTagName(request.getInstanceName());
			webProviderHelper.saveInstanceInfo(instanceInfo, request.getSolutionClientMapId(), request.getSolutionVmMapId(), request.getProviderId());
			logger.info("instanceInfo Information Saved..");
			
			// 5. Update Latest State of Instance By Thread Call if still in progress state
			awsInstanceHelper.instanceStateUpdateByThread(instanceInfo.getInstanceState(), instanceInfo.getInstanceId(), request.getServerCredentialVo());
			
			// 6. Entry For Instance_Cost_Reference.
			Integer instanceInfoId  = webProviderHelper.getInstanceInfoByInstanceId(instance.getInstanceId());
			if(instanceInfoId != null) 
			{
				Integer costDetailsId = jdbcService.getCostDetail(request.getProviderId(), instance.getInstanceType(), ECostType.BASIC.getCostType());
				if(costDetailsId != null) 
				{
					jdbcService.saveCostReference(instanceInfoId, costDetailsId);
				}
			}
			
			// 7. build and return response object
			return awsInstanceHelper.getCreateResponse(instanceInfo);
		}
		catch(Exception e) 
		{
			logger.error("=========================> "+e.getMessage());
			throw new CCDMException(ErrorCodes.AWS_PROVIDER_SERVICE_REQUEST_FAILED);
		}
	}

	@Override
	public InstanceStatusResponse describe(String instanceId, ServerCredentialVo serverCredentialVo) throws CCDMException 
	{
		DescribeInstancesRequest request = awsInstanceHelper.getDescribeInstancesRequest(instanceId);
		DescribeInstancesResult describeInstancesResult = getAmazonEC2(serverCredentialVo).describeInstances(request);
		InstanceStatusResponse response = awsInstanceHelper.getInstanceStatusResponse(describeInstancesResult);
		return response;
	}

	/* *********************************** Private Methods *********************************** */
	private AmazonEC2 getAmazonEC2(ServerCredentialVo serverCredentialVo) 
	{
		return amazonEC2ClientFactory.getAmazonEC2(serverCredentialVo.getAccessId(), serverCredentialVo.getAccessKey());
	}
}
