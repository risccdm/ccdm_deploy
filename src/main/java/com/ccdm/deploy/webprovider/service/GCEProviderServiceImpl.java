package com.ccdm.deploy.webprovider.service;

import java.util.Date;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.common.enums.EGCERegions;
import com.ccdm.deploy.elastic.solutionhistory.model.ClientInstanceVo;
import com.ccdm.deploy.elastic.solutionhistory.model.CreateInstanceRequestVo;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.exceptions.GCEException;
import com.ccdm.deploy.infrastructure.property.AppCommonProperties;
import com.ccdm.deploy.instance.biz.GCEInstanceResponseAdapter;
import com.ccdm.deploy.instance.constants.InstanceConstant;
import com.ccdm.deploy.provider.constants.GCEConstants;
import com.ccdm.deploy.solution.model.ServerCredentialVo;
import com.ccdm.deploy.webprovider.helper.IGCEInstanceHelper;
import com.ccdm.deploy.webprovider.helper.WebProviderCommonHelper;
import com.ccdm.deploy.webprovider.response.InstanceCreateResponse;
import com.ccdm.deploy.webprovider.response.InstanceStatusResponse;
import com.google.cloud.compute.Address;
import com.google.cloud.compute.AddressInfo;
import com.google.cloud.compute.AttachedDisk;
import com.google.cloud.compute.AttachedDisk.PersistentDiskConfiguration;
import com.google.cloud.compute.Compute;
import com.google.cloud.compute.DiskId;
import com.google.cloud.compute.DiskInfo;
import com.google.cloud.compute.ImageDiskConfiguration;
import com.google.cloud.compute.ImageId;
import com.google.cloud.compute.Instance;
import com.google.cloud.compute.InstanceId;
import com.google.cloud.compute.InstanceInfo;
import com.google.cloud.compute.MachineTypeId;
import com.google.cloud.compute.NetworkId;
import com.google.cloud.compute.NetworkInterface;
import com.google.cloud.compute.NetworkInterface.AccessConfig;
import com.google.cloud.compute.Operation;
import com.google.cloud.compute.RegionAddressId;

@Component("gceProvider")
public class GCEProviderServiceImpl implements IProviderService 
{
	private static final Logger logger = LoggerFactory.getLogger(GCEProviderServiceImpl.class);
	
	private static final Integer MOCKVALUE = 1;
	private static final Integer REALVALUE = 2;
	
	@Autowired
	private IGCEInstanceHelper gceInstanceHelper;
	
	@Autowired
	private AppCommonProperties appCommonProperties;
	
	@Autowired
	private GCEInstanceResponseAdapter gceInstanceResponseAdapter;
	
	@Autowired
	private WebProviderCommonHelper webProviderHelper;
	
	@Override
	public InstanceCreateResponse create(CreateInstanceRequestVo creatInstanceRequest) throws CCDMException {
		if(MOCKVALUE.equals(GceRelaOrMock())) {
			return buildInstanceCreateResponse(creatInstanceRequest);
		}
		String instanceName = creatInstanceRequest.getInstanceName().toLowerCase();
		InstanceInfo info = buildGceInstanceInfo(gceInstanceHelper.getCompute(), instanceName,
				creatInstanceRequest.getInstanceConfig().getInstanceType(), creatInstanceRequest.getInstanceConfig().getImageId(), creatInstanceRequest.getInstanceConfig().getImageProject());
		gceInstanceHelper.create(info);
		Instance instance = gceInstanceHelper.describe(info.getInstanceId());
		// 4. Save Instance
		com.ccdm.deploy.vo.InstanceInfo instanceInfo = buildInstanceInfo(instance, creatInstanceRequest.getInstanceConfig().getPemKey());
		instanceInfo.setTagName(creatInstanceRequest.getInstanceName());
		webProviderHelper.saveInstanceInfo(instanceInfo, creatInstanceRequest.getSolutionClientMapId(), creatInstanceRequest.getSolutionVmMapId(), creatInstanceRequest.getInstanceConfig().getPlatformRefId());
		logger.info("..instanceInfo Information Saved..");
		return gceInstanceResponseAdapter.buildCreateResponse(instance, creatInstanceRequest.getInstanceConfig().getPemKey());
	}

	private com.ccdm.deploy.vo.InstanceInfo buildInstanceInfo(Instance instance, String keyName) {
		com.ccdm.deploy.vo.InstanceInfo instanceInfo = new com.ccdm.deploy.vo.InstanceInfo();
		instanceInfo.setInstanceId(instance.getGeneratedId());
		instanceInfo.setTagName(instance.getInstanceId().getInstance());
		instanceInfo.setInstanceState(instance.getStatus().name().toLowerCase());
		instanceInfo.setInstanceType(instance.getMachineType().getType());
		instanceInfo.setKeyName(keyName);
		instanceInfo.setLaunchTime(new Date(instance.getCreationTimestamp()));
		instanceInfo.setPort(InstanceConstant.INSTANCE_DEFAULT_PORT_NUMBER);
		instanceInfo.setPublicDns(instance.getNetworkInterfaces().get(0).getAccessConfigurations().get(0).getNatIp());
		instanceInfo.setPublicIpv4(instance.getNetworkInterfaces().get(0).getAccessConfigurations().get(0).getNatIp());
		return instanceInfo;
	}

	@Override
	public InstanceStatusResponse describe(String instanceId, ServerCredentialVo serverCredentialVo) throws CCDMException {
		InstanceId gceInstanceId = buildGCEInstanceId(instanceId);
		return gceInstanceResponseAdapter.buildStatusResponse(gceInstanceHelper.describe(gceInstanceId));
	}

	private DiskId getImageId(Compute compute, String imageFamily, String imageProject, String instanceName) throws GCEException {
		ImageId imageId = ImageId.of(imageProject, imageFamily);
		DiskId diskId = DiskId.of(GCEConstants.GCE_US_CENTRAL1_A, GCEConstants.GCE_DISK + instanceName);
		ImageDiskConfiguration diskConfiguration = ImageDiskConfiguration.of(imageId);
		DiskInfo disk = DiskInfo.of(diskId, diskConfiguration);
		com.google.cloud.compute.Operation operation = compute.create(disk);
		// Wait for operation to complete
		try {
			operation = operation.waitFor();
		} catch (InterruptedException | TimeoutException e) {
			throw new GCEException("GCE Disk creation failed");
		}
		if (operation.getErrors() == null) {
		  logger.info("Disk " + diskId + " was successfully created");
		  return diskId;
		} else {
		  throw new GCEException("GCE Disk creation failed");
		}
	}
	
	private RegionAddressId createRegion(Compute compute, String instanceName) throws GCEException  {
		RegionAddressId addressId = RegionAddressId.of(EGCERegions.USCENTRAL1.getGCERegionName(), GCEConstants.GCE_REGION_ADDRESS + instanceName);
		Operation operation = compute.create(AddressInfo.of(addressId));
		// Wait for operation to complete
		try {
			operation = operation.waitFor();
		} catch (InterruptedException | TimeoutException e) {
			throw new GCEException("GCE Address creation failed");
		}
		if (operation.getErrors() == null) {
		  logger.info("Address " + addressId + " was successfully created");
		  return addressId;
		} else {
		  throw new GCEException("GCE Address creation failed");
		}
	}
	
	private InstanceInfo buildGceInstanceInfo(Compute compute, String instanceName, String machineType, String imageFamily, String imageProject) throws CCDMException {
		Address externalIp = compute.getAddress(createRegion(compute, instanceName));
		InstanceId instanceId = InstanceId.of(GCEConstants.GCE_US_CENTRAL1_A, instanceName);
		NetworkId networkId = NetworkId.of(GCEConstants.GCE_DEFAULT_NERWORK);
		PersistentDiskConfiguration attachConfiguration =
		    PersistentDiskConfiguration.newBuilder(getImageId(compute, imageFamily, imageProject,instanceName)).setBoot(true).build();
		AttachedDisk attachedDisk = AttachedDisk.of("dev_"+instanceName, attachConfiguration);
		NetworkInterface networkInterface = NetworkInterface.newBuilder(networkId) .setAccessConfigurations(AccessConfig.of(externalIp.getAddress())).build();
		MachineTypeId machineTypeId = MachineTypeId.of(GCEConstants.GCE_US_CENTRAL1_A, machineType);
		InstanceInfo instanceInfo = InstanceInfo.of(instanceId, machineTypeId, attachedDisk, networkInterface);
		return instanceInfo;
	}
	
	public InstanceId buildGCEInstanceId(String instanceId) {
		InstanceId instance = InstanceId.of(GCEConstants.GCE_US_CENTRAL1_A, instanceId);
		return instance;
	}
	
	private Integer GceRelaOrMock() {
		try {
			Integer mockOrReal = Integer.valueOf(appCommonProperties.getGceInstanceRealOrMock());
			return mockOrReal;
		} catch (Exception e) {
			return REALVALUE;
		}
	}
	
	private InstanceCreateResponse buildInstanceCreateResponse(CreateInstanceRequestVo creatInstanceRequest){
		ClientInstanceVo clientInstanceVo = new ClientInstanceVo();
		clientInstanceVo.setInstanceId(creatInstanceRequest.getInstanceName());
		clientInstanceVo.setInstanceState(Operation.Status.DONE.name());
		clientInstanceVo.setInstanceName(creatInstanceRequest.getInstanceName());
		clientInstanceVo.setPublicDns(creatInstanceRequest.getInstanceName());
		clientInstanceVo.setPublicIpv4(creatInstanceRequest.getInstanceName());
		return new InstanceCreateResponse(clientInstanceVo);
	}
}
