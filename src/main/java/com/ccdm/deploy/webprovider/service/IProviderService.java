package com.ccdm.deploy.webprovider.service;

import com.ccdm.deploy.elastic.solutionhistory.model.CreateInstanceRequestVo;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.solution.model.ServerCredentialVo;
import com.ccdm.deploy.webprovider.response.InstanceCreateResponse;
import com.ccdm.deploy.webprovider.response.InstanceStatusResponse;

public interface IProviderService 
{
	InstanceCreateResponse create(CreateInstanceRequestVo request) throws CCDMException;

	InstanceStatusResponse describe(String instanceId, ServerCredentialVo serverCredentialVo) throws CCDMException;
}
