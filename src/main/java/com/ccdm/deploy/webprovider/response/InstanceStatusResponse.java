package com.ccdm.deploy.webprovider.response;

public class InstanceStatusResponse 
{
	private String instanceId;
	private String currentState;
	
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getCurrentState() {
		return currentState;
	}
	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}
}
