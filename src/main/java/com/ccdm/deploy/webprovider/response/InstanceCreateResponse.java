package com.ccdm.deploy.webprovider.response;

import com.ccdm.deploy.elastic.solutionhistory.model.ClientInstanceVo;

public class InstanceCreateResponse {

	private ClientInstanceVo clientInstanceVo;

	public InstanceCreateResponse() {}
	
	public InstanceCreateResponse(ClientInstanceVo clientInstanceVo) {
		this.clientInstanceVo = clientInstanceVo;
	}

	public ClientInstanceVo getClientInstanceVo() {
		return clientInstanceVo;
	}

	public void setClientInstanceVo(ClientInstanceVo clientInstanceVo) {
		this.clientInstanceVo = clientInstanceVo;
	}
}
