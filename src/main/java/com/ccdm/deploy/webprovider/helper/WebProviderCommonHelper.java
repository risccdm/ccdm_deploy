package com.ccdm.deploy.webprovider.helper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.common.constants.SqlConstants;
import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.infrastructure.config.JdbcConnnectionProvider;
import com.ccdm.deploy.vo.InstanceInfo;

@Component
public class WebProviderCommonHelper 
{
	private static final Logger logger = LoggerFactory.getLogger(WebProviderCommonHelper.class);
	
	@Autowired
	private JdbcConnnectionProvider jdbcConnnectionProvider;
	
	public void saveInstanceInfo(InstanceInfo instanceInfo, Integer solutionClientMapId, Integer solutionVmMapId, Integer platformRefId) throws CCDMException 
	{
		Connection connection = null;
		try {
				//JDBC Connection Save
				connection = jdbcConnnectionProvider.getJdbcConnection();
				PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement(insertQueryForInstaneInfo());
				setValuetoStatement(instanceInfo.getTagName(), preparedStatement, 1);
				setValuetoStatement(instanceInfo.getInstanceId(), preparedStatement, 2);
				setValuetoStatement(instanceInfo.getInstanceType(), preparedStatement, 3);
				setValuetoStatement(instanceInfo.getInstanceState(), preparedStatement, 4);
				setValuetoStatement(instanceInfo.getKeyName(), preparedStatement, 5);
				setValuetoStatement(instanceInfo.getLaunchTime(), preparedStatement, 6);
				/*setValuetoStatement(instanceInfo.getLaunchTime(), preparedStatement, 6);*/
				setValuetoStatement(instanceInfo.getSecurityGroup() == null ? "" : instanceInfo.getSecurityGroup(), preparedStatement, 7);
				setValuetoStatement(instanceInfo.getPublicDns(), preparedStatement, 8);
				setValuetoStatement(instanceInfo.getPort(), preparedStatement, 9);
				setValuetoStatement(instanceInfo.getPublicIpv4(), preparedStatement, 10);
				setValuetoStatement(solutionClientMapId, preparedStatement, 11);
				setValuetoStatement(solutionVmMapId, preparedStatement, 12);
				setValuetoStatement(platformRefId, preparedStatement, 13);
				preparedStatement.execute();
				logger.info("instance saved...!");
		} 
		catch (Exception e) {
			logger.error(e.getMessage());
			throw new CCDMException(ErrorCodes.INTERNAL_ERROR);
		}
		try {
			if(!connection.isClosed()) {
				connection.close();
				logger.info("JDBC connection Closed.!");
			}
		} catch (SQLException e) {
			logger.error("Exception Occur while Closing JDBC Connection After InstanceInfo Saved!!!");
			throw new CCDMException(ErrorCodes.INTERNAL_ERROR);
		}
	}
	
	/*private static java.sql.Timestamp getCurrentTimeStamp(java.util.Date launchTime) 
	{
		logger.info("time --------------------------------------> "+new java.sql.Timestamp(launchTime.getTime()));
		return new java.sql.Timestamp(launchTime.getTime());
		
	}*/
	
	public Integer getInstanceInfoByInstanceId(String instanceId) throws CCDMException 
	{
		Connection connection = jdbcConnnectionProvider.getJdbcConnection();
		Statement stmt = null;
		try 
		{
			stmt = (Statement) connection.createStatement();
			String sql = SqlConstants.SELECT_ID_FROM_INSTANCE_INFO_USING_INSTANCEID + "'"+instanceId+"'";
		    ResultSet resultSet = stmt.executeQuery(sql);
		    if(resultSet.next()) {
		    	return resultSet.getInt("ID");
		    }
		} 
		catch (SQLException e) 
		{
			logger.error("Sql Exception While Getting InstnceInfo Id Using JDBC Connection ====>>> "+e.getMessage());
		} 
		finally
		{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		        	 connection.close();
		      }catch(SQLException se){
		      }// do nothing
		      try{
		         if(connection!=null)
		        	 connection.close();
		      }catch(SQLException se){
		    	  logger.error("Sql Exception While Getting InstnceInfo Id Using JDBC Connection ====>>> ",se.getMessage());
		      }//end finally try
		}
		return null;
	}
	
	/* ********************* private Methods ********************** */
	private void setValuetoStatement(Object value, PreparedStatement preparedStatement, Integer index ) throws CCDMException, SQLException {
		if(value instanceof String) {
			preparedStatement.setString(index, (String) value);
			return;
		}
		if(value instanceof Integer) {
			preparedStatement.setInt(index , (int) value);
			return;
		}
		if(value instanceof java.util.Date)
		{
			java.util.Date utilDate = (Date) value;
			preparedStatement.setTimestamp(index, new java.sql.Timestamp(utilDate.getTime()));
			return;
		}
		logger.error("error while setting Value to InstanceInfo using Jdbc Connection");
		throw new CCDMException(ErrorCodes.INVALID_OBJECT_TYPE);
	}

	private String insertQueryForInstaneInfo() {
		String query = "INSERT INTO instance_info(TAG_NAME, INSTANCE_ID, INSTANCE_TYPE, INSTANCE_STATE, KEY_NAME, LAUNCH_TIME, SECURITY_GROUP, PUBLIC_DNS, PORT, PUBLIC_IPv4, SOLUTION_CIENT_MAP_ID, SOLUTION_VM_MAP_ID, PROVIDER_PLATFORM_REF_ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
		return query;
	}
}
