package com.ccdm.deploy.webprovider.helper;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.Tag;
import com.ccdm.deploy.elastic.solutionhistory.model.ClientInstanceVo;
import com.ccdm.deploy.elastic.solutionhistory.model.CreateInstanceRequestVo;
import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.instance.constants.EInstanceSystemState;
import com.ccdm.deploy.instance.constants.InstanceConstant;
import com.ccdm.deploy.instance.threads.InstanceStateUpdateThreads;
import com.ccdm.deploy.solution.model.ServerCredentialVo;
import com.ccdm.deploy.vo.InstanceInfo;
import com.ccdm.deploy.webprovider.response.InstanceCreateResponse;
import com.ccdm.deploy.webprovider.response.InstanceStatusResponse;

@Component
public class AWSInstanceHelper
{
	private static final Logger logger = LoggerFactory.getLogger(AWSInstanceHelper.class);
	
	@Autowired
	private InstanceStateUpdateThreads instanceStateUpdateThread;
	
	public RunInstancesRequest getCreateRunInstanceRequest(CreateInstanceRequestVo request) throws CCDMException 
	{
		RunInstancesRequest runInstancesRequest = new RunInstancesRequest()
    	        .withInstanceType(request.getInstanceConfig().getInstanceType())
    	        .withImageId(request.getInstanceConfig().getImageId())
    	        .withMinCount(1)
    	        .withMaxCount(1)
    	        .withKeyName(request.getInstanceConfig().getPemKey());
		runInstancesRequest.setSecurityGroups(getSecurityGroups(request.getInstanceConfig().getSecurityGroup()));
		return runInstancesRequest;
	}

	public InstanceCreateResponse getCreateResponse(InstanceInfo instanceInfo) throws CCDMException 
	{
		ClientInstanceVo clientInstanceVo = new ClientInstanceVo();
		clientInstanceVo.setInstanceId(instanceInfo.getInstanceId());
		clientInstanceVo.setInstanceState(instanceInfo.getInstanceState());
		clientInstanceVo.setKeyName(instanceInfo.getKeyName());
		clientInstanceVo.setLaunchTime(instanceInfo.getLaunchTime());
		clientInstanceVo.setPort(instanceInfo.getPort());
		clientInstanceVo.setPublicDns(instanceInfo.getPublicDns());
		clientInstanceVo.setPublicIpv4(instanceInfo.getPublicIpv4());
		clientInstanceVo.setInstanceName(instanceInfo.getTagName());
		
		InstanceCreateResponse response = new InstanceCreateResponse();
		response.setClientInstanceVo(clientInstanceVo);
		return response;
	}

	public CreateTagsRequest getCreateTagRequest(String instanceId, String tagName) throws CCDMException 
	{
		return new CreateTagsRequest().withResources(instanceId)
		 					   		  .withTags(new Tag(InstanceConstant.TAG_NAME_KEY, tagName));
	}

	public DescribeInstancesRequest getDescribeInstancesRequest(String instanceId) throws CCDMException 
	{
		DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest();
		describeInstancesRequest.setInstanceIds(getInstanceIds(instanceId));
		return describeInstancesRequest;
	}
	
	public Instance getInstanceFromReservation(Reservation reservation)
	{
		// it will return only one insance if we made one instance create request
		List<Instance> instances = reservation.getInstances();
		return instances.get(0);
	}
	
	public InstanceInfo buildInstanceInfo(Instance instance) 
	{
		InstanceInfo instanceInfo = new InstanceInfo();
		instanceInfo.setInstanceId(instance.getInstanceId());
		instanceInfo.setInstanceState(instance.getState().getName());
		instanceInfo.setInstanceType(instance.getInstanceType());
		instanceInfo.setKeyName(instance.getKeyName());
		instanceInfo.setLaunchTime(instance.getLaunchTime());
		instanceInfo.setPort(InstanceConstant.INSTANCE_DEFAULT_PORT_NUMBER);
		instanceInfo.setPublicDns(instance.getPublicDnsName());
		instanceInfo.setPublicIpv4(instance.getPublicIpAddress());
		instanceInfo.setSecurityGroup(instance.getSecurityGroups().get(0).getGroupName());
		return instanceInfo;
	}
	
	public void instanceStateUpdateByThread(String newInstanceState, String instanceId, ServerCredentialVo serverCredentialVo) throws CCDMException 
	{
		try
		{
			if(newInstanceState != null && !newInstanceState.isEmpty())
			{
				if( newInstanceState.equalsIgnoreCase(EInstanceSystemState.PENDING.getInstanceState())
						|| newInstanceState.equalsIgnoreCase(EInstanceSystemState.REBOOTING.getInstanceState())
						|| newInstanceState.equalsIgnoreCase(EInstanceSystemState.STOPPING.getInstanceState())
						|| newInstanceState.equalsIgnoreCase(EInstanceSystemState.SHUTTING_DOWN.getInstanceState()))
				{
					instanceStateUpdateThread.initiateInstancStateUpdateThread(instanceId, serverCredentialVo);
				}
				else {
					logger.info("Instance State already "+newInstanceState+" State. No need to Update by thread.");
				}
			}
		}
		catch(Exception e) {
			logger.error(ErrorCodes.CANNOT_UPDATE_INSTANCE_STATE.getErrorMessage()+" REASON : "+e.getMessage());
		}
	}
	
	/* *********************************** Private Methods *********************************** */
	private List<String> getInstanceIds(String instanceId) 
	{
		List<String> instanceIds = new ArrayList<>();
		instanceIds.add(instanceId);
		return instanceIds;
	}
	
	private List<String> getSecurityGroups(String securityGroup) 
	{
		List<String> securityGroups = new ArrayList<>();
		securityGroups.add(securityGroup);
		return securityGroups;
	}

	public InstanceStatusResponse getInstanceStatusResponse(DescribeInstancesResult describeInstancesResult) 
	{
		Instance instance = getInstanceFromReservation(describeInstancesResult.getReservations().get(0));
		InstanceStatusResponse response = new InstanceStatusResponse();
		response.setInstanceId(instance.getInstanceId());
		response.setCurrentState(instance.getState().getName());
		return response;
	}
}
