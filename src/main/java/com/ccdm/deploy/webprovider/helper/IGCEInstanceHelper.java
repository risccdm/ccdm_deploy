package com.ccdm.deploy.webprovider.helper;

import java.util.concurrent.TimeoutException;

import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.exceptions.GCEException;
import com.google.cloud.compute.Compute;
import com.google.cloud.compute.Instance;
import com.google.cloud.compute.InstanceId;
import com.google.cloud.compute.InstanceInfo;
import com.google.cloud.compute.Operation;

public interface IGCEInstanceHelper {

	Operation create(InstanceInfo instanceInfo) throws CCDMException;

	Instance describe(InstanceId instanceId) throws CCDMException;

	Compute getCompute() throws CCDMException;
	
	default Operation waitfor(Operation operation) throws GCEException {
		try {
			operation = operation.waitFor();
			if (operation.getErrors() == null) {
				return operation;
			} else {
				throw new GCEException(ErrorCodes.GCE_OPERATION_WAITFOR_EXCEPTION);
			}
		} catch (InterruptedException | TimeoutException e) {
			throw new GCEException(ErrorCodes.GCE_OPERATION_WAITFOR_EXCEPTION);
		}
	}
}
