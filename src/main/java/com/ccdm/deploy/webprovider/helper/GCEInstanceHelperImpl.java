package com.ccdm.deploy.webprovider.helper;

import org.springframework.stereotype.Component;

import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.provider.client.factory.GoogleCloudGCEClientFactory;
import com.google.cloud.compute.Compute;
import com.google.cloud.compute.Instance;
import com.google.cloud.compute.InstanceId;
import com.google.cloud.compute.InstanceInfo;
import com.google.cloud.compute.Operation;

@Component
public class GCEInstanceHelperImpl extends GoogleCloudGCEClientFactory implements IGCEInstanceHelper {

	@Override
	public Operation create(InstanceInfo instanceInfo) throws CCDMException {
		return waitfor(getCompute().create(instanceInfo));
	}

	@Override
	public Instance describe(InstanceId instanceId) throws CCDMException {
		return getCompute().getInstance(instanceId);
	}

	@Override
	public Compute getCompute() throws CCDMException {
		return getGoogleCloudGCECompute();
	}
}
