package com.ccdm.deploy.infrastructure.config;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

import com.ccdm.deploy.solution.thread.factory.DeploySolutionInitiator;

public class CustomAsyncExceptionHandler implements AsyncUncaughtExceptionHandler
{
	private static Logger logger = LoggerFactory.getLogger(DeploySolutionInitiator.class);
	
	@Override
	public void handleUncaughtException(Throwable ex, Method method, Object... params) 
	{
		logger.error("Async Exception message =====> " + ex.getMessage());
		logger.error("Method Name =====> " + method.getName());
		for(Object param : params)
		{
			logger.error("Parameter Value =====> " + param);
		}
	}
}
