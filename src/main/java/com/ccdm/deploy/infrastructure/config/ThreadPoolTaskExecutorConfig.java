package com.ccdm.deploy.infrastructure.config;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.ccdm.deploy.property.EnvironmentProperties;

@Configuration
@EnableAsync
public class ThreadPoolTaskExecutorConfig implements AsyncConfigurer
{
	@Autowired
	private EnvironmentProperties environmentProperties; 
	
	@Bean(name = "deployTaskExecutor")
	public ThreadPoolTaskExecutor getAsyncExecutor() 
	{
		ThreadPoolTaskExecutor executor  = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(Integer.parseInt(environmentProperties.getCorePoolSize()));
		executor.setMaxPoolSize(Integer.parseInt(environmentProperties.getMaxPoolSize()));
		executor.setThreadNamePrefix(environmentProperties.getThreadNamePrefix());
		executor.setWaitForTasksToCompleteOnShutdown(Boolean.parseBoolean(environmentProperties.getWaitForTaskToCompleteOnShutdown()));
		executor.initialize();
		return executor;
	}

	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() 
	{
		return new CustomAsyncExceptionHandler();
	}
}
