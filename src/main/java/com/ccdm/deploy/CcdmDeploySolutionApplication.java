package com.ccdm.deploy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class CcdmDeploySolutionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CcdmDeploySolutionApplication.class, args);
	}
}
