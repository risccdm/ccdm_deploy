package com.ccdm.deploy.instance.service;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ccdm.deploy.agent.constants.IcingaConstants;
import com.ccdm.deploy.agent.model.ExecutionScriptRequest;
import com.ccdm.deploy.agent.service.AgentService;
import com.ccdm.deploy.common.validator.CommonValidator;
import com.ccdm.deploy.elastic.solutionhistory.model.AgentClientRequestVo;
import com.ccdm.deploy.elastic.solutionhistory.model.AgentMasterConfigRequestVo;
import com.ccdm.deploy.elastic.solutionhistory.model.CreateInstanceRequestVo;
import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.provider.validator.InstanceValidator;
import com.ccdm.deploy.webServer.factory.WebServerProviderFactory;
import com.ccdm.deploy.webprovider.response.InstanceCreateResponse;
import com.ccdm.deploy.webprovider.service.IProviderService;

@Service
@Transactional(readOnly = false)
public class InstanceServicecImpl implements InstanceService
{
	private static final Logger logger = LoggerFactory.getLogger(InstanceServicecImpl.class);
	
	@Autowired
	private AgentService agentService;
	@Autowired
	private WebServerProviderFactory webServerProviderFactory;
	
	@Override
	public InstanceCreateResponse createInstance(CreateInstanceRequestVo request) throws CCDMException  
	{
		CommonValidator.validateObject(request, ErrorCodes.INVALID_CREATE_INSTACE_REQUEST);
		CommonValidator.validateString(request.getProviderName(), ErrorCodes.INVALID_PROVIDER_NAME);
		
		IProviderService providerService =  webServerProviderFactory.getWebProviderByProviderName(request.getProviderName());
		InstanceCreateResponse response = providerService.create(request);
		return response;
	}

	@Override
	public String configIcingaMaster(AgentMasterConfigRequestVo masterAgentConfigRequest) throws CCDMException
	{
		String ticketSalt = "";
		try
		{
			InstanceValidator.validateMasterAgentRequest(masterAgentConfigRequest);
			InstanceValidator.validateServerCredentialVo(masterAgentConfigRequest.getServerCredentialVo());
			
			ExecutionScriptRequest executionScriptRequest = new ExecutionScriptRequest();
			executionScriptRequest.setInstanceVo(masterAgentConfigRequest.getMasterInstanceVo());
			executionScriptRequest.setServerCredentialVo(masterAgentConfigRequest.getServerCredentialVo());
			executionScriptRequest.setScript(getScriptForIcingaMaster(masterAgentConfigRequest));
			
			logger.info("Config in Master Agent...");
			ticketSalt = agentService.configAgent(executionScriptRequest);
			logger.info("Config in Master Completed.... ticketSalt : "+ticketSalt);
		}
		catch (Exception e) 
		{
			logger.error(e.getMessage());
			throw new CCDMException(ErrorCodes.ERROR_CLIENT_ICINGA_CONFIG);
		}
		if(ticketSalt.isEmpty()) {
			throw new CCDMException(ErrorCodes.INVALID_TICKET_SALT);
		}
		return ticketSalt;
	}
	
	@Override
	public String installIcingaClient(AgentClientRequestVo clientAgentRequest) throws CCDMException
	{
		try
		{
			InstanceValidator.validateClientAgentRequest(clientAgentRequest);
			InstanceValidator.validateServerCredentialVo(clientAgentRequest.getServerCredentialVo());
			
			logger.info("Uploading Icinga Installation Script file to Client Instance..");
			agentService.uploadFile(clientAgentRequest);
			
			ExecutionScriptRequest executionScriptRequest = new ExecutionScriptRequest();
			executionScriptRequest.setInstanceVo(clientAgentRequest.getClientInstanceVo());
			executionScriptRequest.setServerCredentialVo(clientAgentRequest.getServerCredentialVo());
			executionScriptRequest.setScript(getScriptToRunUploadedFile(clientAgentRequest));
			
			logger.info("Icinga Installation Started...");
			String output = agentService.installAgent(executionScriptRequest);
			logger.info("Icinga Installation Completed...");
			return output;
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
			throw new CCDMException(ErrorCodes.ERROR_CLIENT_ICINGA_CONFIG);
		}
	}
	
	private String getScriptToRunUploadedFile(AgentClientRequestVo clientAgentRequest) 
	{
		StringBuffer commandBuff = new StringBuffer();
		// File execute Permission Command
		commandBuff.append(IcingaConstants.EXEC_PERMISSION_COMMAND);
		// Remove Trailing space in Script file Command
		commandBuff.append(IcingaConstants.REMOVE_TRAILING_CHAR_COMMAND);
		// File execute Command
		commandBuff.append(IcingaConstants.CLIENT_COMMAND.replaceAll(IcingaConstants.CLIENT_INSTANCE_NAME, clientAgentRequest.getClientInstanceVo().getInstanceName())
				   										 .replaceAll(IcingaConstants.TICKET_SALT, clientAgentRequest.getTicketSalt()));
		String finalCommand = commandBuff.toString().replaceAll(IcingaConstants.USER, clientAgentRequest.getServerCredentialVo().getUser());
		System.out.println("run file command : "+finalCommand);
		return finalCommand;
	}
	
	private String getScriptForIcingaMaster(AgentMasterConfigRequestVo masterAgentConfigRequest) 
	{
		String command = IcingaConstants.MASTER_COMMAND.replaceAll(IcingaConstants.USER, masterAgentConfigRequest.getServerCredentialVo().getUser())
												.replaceAll(IcingaConstants.CLIENT_INSTANCE_NAME, masterAgentConfigRequest.getClientInstanceVo().getInstanceName())
												.replaceAll(IcingaConstants.CLIENT_IP_ADDRESS, masterAgentConfigRequest.getClientInstanceVo().getPublicIpv4());
		logger.info("Master Command : "+command);
		
		return command;
	}
}
