package com.ccdm.deploy.instance.service;

import com.ccdm.deploy.elastic.solutionhistory.model.AgentClientRequestVo;
import com.ccdm.deploy.elastic.solutionhistory.model.AgentMasterConfigRequestVo;
import com.ccdm.deploy.elastic.solutionhistory.model.CreateInstanceRequestVo;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.webprovider.response.InstanceCreateResponse;

public interface InstanceService 
{
	InstanceCreateResponse createInstance(CreateInstanceRequestVo creatInstanceRequestVo) throws CCDMException;

	String configIcingaMaster(AgentMasterConfigRequestVo masterAgentConfigReques) throws CCDMException;

	String installIcingaClient(AgentClientRequestVo clientAgentRequest) throws CCDMException;
}
