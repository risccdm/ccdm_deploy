package com.ccdm.deploy.instance.threads;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.instance.Processor.InstanceJdbcProcessor;
import com.ccdm.deploy.instance.constants.EInstanceSystemState;
import com.ccdm.deploy.instance.constants.InstanceConstant;
import com.ccdm.deploy.solution.model.ServerCredentialVo;
import com.ccdm.deploy.webprovider.response.InstanceStatusResponse;
import com.ccdm.deploy.webprovider.service.IProviderService;

@Component
public class InstanceStateUpdateThreads 
{
	private static final Logger logger = LoggerFactory.getLogger(InstanceStateUpdateThreads.class);
	
	@Autowired
	private InstanceJdbcProcessor instanceJdbcProcessor;
	@Autowired
	@Qualifier("awsProvider")
	private IProviderService providerService;
	
	public boolean updateInstanceState(String instanceId, ServerCredentialVo serverCredentialVo) throws CCDMException
	{
		boolean updateFlag = false;
		try
		{
			logger.info("Updating Instance State will start within 20 sec...");
			Thread.sleep(20000);
			logger.info("Updating Instance State Thread started...");
			
			InstanceStatusResponse response =  providerService.describe(instanceId, serverCredentialVo);
			
			if(response.getCurrentState().equalsIgnoreCase(EInstanceSystemState.STOPPED.getInstanceState()) 
					|| response.getCurrentState().equalsIgnoreCase(EInstanceSystemState.TERMINATED.getInstanceState()) 
					|| response.getCurrentState().equalsIgnoreCase(EInstanceSystemState.RUNNING.getInstanceState()))
			{
				instanceJdbcProcessor.updateInstanceState(response.getInstanceId(), response.getCurrentState());
				updateFlag = true;
			}
		}
		catch(Exception e) {
			logger.error(e.getMessage());
			return false;
		}
		return updateFlag;
	}

	public void initiateInstancStateUpdateThread(String instanceId, ServerCredentialVo serverCredentialVo) 
	{
		 new Thread(new Runnable() 
		 {
				@Override
				public void run() 
				{
					try 
					{
						boolean updateFlag = true;
						long initialTimeout = Calendar.getInstance().getTimeInMillis();
						long timeDiff = 0;
						
						while(updateFlag && (timeDiff < InstanceConstant.MAX_INSTANCE_UPDATE_TIMEOUT))
						{
							logger.info("Updating Instance State Thread Initiated...");
							updateFlag = !updateInstanceState(instanceId, serverCredentialVo);
							logger.info("Updating Instance State Thread Completed...");
							
							long currentTimeout = Calendar.getInstance().getTimeInMillis();
							timeDiff = (currentTimeout - initialTimeout);
						}
					} 
					catch (Exception e) 
					{
						logger.error(e.getMessage());
					}
				}
		 }).start();
	}
}
