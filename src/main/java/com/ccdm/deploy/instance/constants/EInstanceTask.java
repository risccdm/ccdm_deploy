package com.ccdm.deploy.instance.constants;

public enum EInstanceTask {

	CREATE_INSTANCE("CreateInstance"),
	MASTER_AGENT_CONFIG("MasterAgentConfig"),
	CLIENT_AGENT_INSTALL("ClientAgentInstall"),
	SCRIPT_EXECUTION("ScriptExecution");
	
	private String instanceTaskName;
	
	private EInstanceTask(String instanceTaskName) {
		this.instanceTaskName = instanceTaskName;
	}
	
	public String getTaskName() {
		return this.instanceTaskName;
	}
}
