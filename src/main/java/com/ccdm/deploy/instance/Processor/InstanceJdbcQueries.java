package com.ccdm.deploy.instance.Processor;

import org.springframework.stereotype.Component;

@Component
public class InstanceJdbcQueries 
{
	// Update Instance Status
	public static final String INSTANCE_STATE_PARAM 	= "<INSTANCE_STATE_PARAM>";
	public static final String INSTANCE_ID_PARAM 		= "<INSTANCE_ID_PARAM>";
	public final String UPDATE_INSTANCE_STATE_BY_INSTANCE_ID = "UPDATE instance_info SET INSTANCE_STATE = '"+INSTANCE_STATE_PARAM+"' WHERE INSTANCE_ID='"+INSTANCE_ID_PARAM+"'";
	
	// Insert into Instance Cost Reference
	public static final String SAVE_COST_DETAIL_REFERENCE = "INSERT INTO instance_cost_reference(COST_DETAIL_ID , INSTANCE_INFO_ID, CREATION_DATE) VALUES (?,?,?)";
}
