package com.ccdm.deploy.instance.Processor;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.infrastructure.config.JdbcConnnectionProvider;

@Component
public class InstanceJdbcProcessor 
{
	private static final Logger logger = LoggerFactory.getLogger(InstanceJdbcProcessor.class);
	
	@Autowired
	private JdbcConnnectionProvider jdbcConnnectionProvider;
	@Autowired
	private InstanceJdbcQueries instanceJdbcQueries;
	
	public void updateInstanceState(String instanceId, String instanceState) throws CCDMException
	{
		Connection connection = null;
		Statement stmt = null;
		try
		{
			connection = jdbcConnnectionProvider.getJdbcConnection();
			stmt = connection.createStatement();

			// Let us update state of the Instance;
			int rows = stmt.executeUpdate(updateQueryForInstanceInfo(instanceId, instanceState));
			if(rows > 0)
			{
				logger.info("Instance State Successfully Updated..!");
			}
			else
			{
				logger.info("No Update ..! InstanceId : "+instanceId);
			}
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
			throw new CCDMException(ErrorCodes.INTERNAL_ERROR);
		}
		closeJdbcConnection(connection);
	}

	private void closeJdbcConnection(Connection connection) throws CCDMException 
	{
		try 
		{
			if(!connection.isClosed()) 
			{
				connection.close();
				logger.info("JDBC connection Closed.!");
			}
		} 
		catch (SQLException e) 
		{
			logger.error("Exception Occur while Closing JDBC Connection After InstanceInfo Saved!!!");
			throw new CCDMException(ErrorCodes.INTERNAL_ERROR);
		}
	}

	private String updateQueryForInstanceInfo(String instanceId, String instanceState) 
	{
		String query = instanceJdbcQueries.UPDATE_INSTANCE_STATE_BY_INSTANCE_ID
						   .replaceAll(InstanceJdbcQueries.INSTANCE_STATE_PARAM, instanceState)
						   .replaceAll(InstanceJdbcQueries.INSTANCE_ID_PARAM, instanceId);
		logger.info("update query : "+query);
		return query;
	}
}
