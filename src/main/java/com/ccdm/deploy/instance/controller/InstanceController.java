package com.ccdm.deploy.instance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.ccdm.deploy.base.controller.BaseController;
import com.ccdm.deploy.elastic.solutionhistory.model.AgentClientRequestVo;
import com.ccdm.deploy.elastic.solutionhistory.model.AgentMasterConfigRequestVo;
import com.ccdm.deploy.elastic.solutionhistory.model.CreateInstanceRequestVo;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.instance.service.InstanceService;
import com.ccdm.deploy.webprovider.response.InstanceCreateResponse;

@RestController
public class InstanceController extends BaseController 
{
	@Autowired
	private InstanceService instanceService;

	@Override
	protected Class<?> getClassName() {
		return this.getClass();
	}
	
	public InstanceCreateResponse createInstance(CreateInstanceRequestVo creatInstanceRequestVo) throws CCDMException
	{
		return instanceService.createInstance(creatInstanceRequestVo);
	}
	
	public String configIcingaMaster(AgentMasterConfigRequestVo masterAgentConfigRequest) throws CCDMException
	{
		return instanceService.configIcingaMaster(masterAgentConfigRequest);
	}
	
	public String installIcingaClient(AgentClientRequestVo clientAgentRequest) throws CCDMException
	{
		return instanceService.installIcingaClient(clientAgentRequest);
	}
}
