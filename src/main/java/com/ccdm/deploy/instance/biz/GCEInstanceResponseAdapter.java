package com.ccdm.deploy.instance.biz;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.ccdm.deploy.elastic.solutionhistory.model.ClientInstanceVo;
import com.ccdm.deploy.instance.constants.EInstanceSystemState;
import com.ccdm.deploy.webprovider.response.InstanceCreateResponse;
import com.ccdm.deploy.webprovider.response.InstanceStatusResponse;
import com.google.cloud.compute.Instance;
import com.google.cloud.compute.InstanceInfo.Status;

@Component
public class GCEInstanceResponseAdapter {

	public InstanceCreateResponse buildCreateResponse(Instance instance, String keyName) {
		ClientInstanceVo clientInstanceVo = new ClientInstanceVo();
		clientInstanceVo.setInstanceId(instance.getGeneratedId());
		clientInstanceVo.setInstanceName(instance.getInstanceId().getInstance());
		String status = instance.getStatus().name().equals(Status.RUNNING.name()) ? EInstanceSystemState.RUNNING.getInstanceState() : EInstanceSystemState.PENDING.getInstanceState();
		clientInstanceVo.setInstanceState(status);
		clientInstanceVo.setLaunchTime(new Date(instance.getCreationTimestamp()));
		clientInstanceVo.setKeyName(keyName);
		clientInstanceVo.setPublicIpv4(instance.getNetworkInterfaces().get(0).getAccessConfigurations().get(0).getNatIp());
		clientInstanceVo.setPublicDns(instance.getNetworkInterfaces().get(0).getAccessConfigurations().get(0).getNatIp());
		return new InstanceCreateResponse(clientInstanceVo);
	}

	public InstanceStatusResponse buildStatusResponse(Instance instance) {
		InstanceStatusResponse instanceStatusResponse = new InstanceStatusResponse();
		instanceStatusResponse.setCurrentState(instance.getStatus().name().toLowerCase());
		instanceStatusResponse.setInstanceId(instance.getInstanceId().getInstance());
		return instanceStatusResponse;
	}
}
