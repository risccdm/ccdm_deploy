package com.ccdm.deploy.init;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.solution.thread.factory.DeploySolutionManager;

@Component
public class PostConstructInitializer 
{
	@Autowired
	private DeploySolutionManager deploySolutionManager;
	
	@PostConstruct
	public void initializeDeploySolutionMonitor()
	{
		deploySolutionManager.manageProviderTask();
	}
}
