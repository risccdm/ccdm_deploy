package com.ccdm.deploy.elastic.repository.scriptlog;

import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import com.ccdm.deploy.elastic.vo.ScriptOutputLog;

public interface IScriptOutputLogRepository extends ElasticsearchCrudRepository<ScriptOutputLog, String> {

}
