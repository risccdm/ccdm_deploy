package com.ccdm.deploy.elastic.repository.solutionhistory;

import java.util.List;

import com.ccdm.deploy.elastic.vo.SolutionHistory;

public interface ISolutionHistoryRepoCustom 
{
	List<SolutionHistory> findAllBySolutionIdAndCreatedForAndStatusAndOrderBySeqNo(Integer solutionId, String createdFor, String status);

	List<String> getInstanceUniqueIdsBySolutionStateAndOrder(String solutionState, boolean order);
}
