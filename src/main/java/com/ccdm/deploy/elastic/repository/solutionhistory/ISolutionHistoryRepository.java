package com.ccdm.deploy.elastic.repository.solutionhistory;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.ccdm.deploy.elastic.vo.SolutionHistory;

public interface ISolutionHistoryRepository extends CrudRepository<SolutionHistory, String> 
{
	List<SolutionHistory> findAllBySolutionIdAndCreatedFor(Integer solutionId, String createdFor);
	
	List<SolutionHistory> findAllByInstanceUniqueId(String instanceUniqueId);
	
	SolutionHistory findOneByInstanceTaskNameAndInstanceUniqueId(String instanceTaskName, String instanceUniqueId);
	
	List<SolutionHistory> findAllBySolutionIdAndCreatedForOrderBySeqNo(Integer solutionId, String createdFor);
	
	List<SolutionHistory> findAllBySolutionId(Integer solutionId);

	List<SolutionHistory> findAllByCreatedFor(String deployFor);
	
	List<SolutionHistory> findAllByOrderBySeqNoDesc();
	
	List<SolutionHistory> findAllByInstanceUniqueIdOrderBySeqNoAsc(String instanceUniqueId);
	
	List<SolutionHistory> findAllByInstanceUniqueIdAndStatusOrderBySeqNoAsc(String deployUniqueId, String Status);
}
