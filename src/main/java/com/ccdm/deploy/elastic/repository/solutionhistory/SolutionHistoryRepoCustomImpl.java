package com.ccdm.deploy.elastic.repository.solutionhistory;

import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.stereotype.Repository;

import com.ccdm.deploy.elastic.vo.SolutionHistory;
import com.ccdm.deploy.infrastructure.config.ElasticsearchConfiguration;
import com.ccdm.deploy.property.EnvironmentProperties;
import com.ccdm.deploy.solution.constants.SolutionHistoryAggFieldConstants;

@Repository
public class SolutionHistoryRepoCustomImpl implements ISolutionHistoryRepoCustom
{
	@Autowired
	private ElasticsearchOperations esOperation;
	@Autowired
	private ElasticsearchConfiguration esConfiguration;
	@Autowired
	private EnvironmentProperties environmentProperties; 

	@Override
	public List<SolutionHistory> findAllBySolutionIdAndCreatedForAndStatusAndOrderBySeqNo(Integer solutionId, String createdFor, String status) 
	{
		
		CriteriaQuery criteriaQuery = new CriteriaQuery(Criteria.where("solutionId").is(solutionId)
																.and("createdFor").is(createdFor)
																.and("status").is(status))
																.addSort(new Sort(Direction.ASC,"seqNo"));
		return esOperation.queryForList(criteriaQuery, SolutionHistory.class);
	}

	@Override
	public List<String> getInstanceUniqueIdsBySolutionStateAndOrder(String solutionState, boolean order) // true for ASC and false for DESC
	{
		String MAX_DEPLOY_DATE = SolutionHistoryAggFieldConstants.MAX_TERM + SolutionHistoryAggFieldConstants.DEPLOY_DATE_FIELD;
		String GROUP_BY_INSTANCE_UNIQUE_ID = SolutionHistoryAggFieldConstants.GROUP_BY_TERM + SolutionHistoryAggFieldConstants.INSTANCE_UNIQUE_ID_FIELD;
		
		SearchRequestBuilder searchReq = getSolutionhHistoryQueryBuilder();;
		searchReq.setQuery(QueryBuilders.matchQuery(SolutionHistoryAggFieldConstants.STATUS_FIELD, solutionState))
							 		 	.addAggregation(AggregationBuilders.terms(GROUP_BY_INSTANCE_UNIQUE_ID)
							 				 .field(SolutionHistoryAggFieldConstants.INSTANCE_UNIQUE_ID_FIELD)
							 				 .order(Order.aggregation(MAX_DEPLOY_DATE, true))
							 			.subAggregation(AggregationBuilders.max(MAX_DEPLOY_DATE)
											 .field(SolutionHistoryAggFieldConstants.DEPLOY_DATE_FIELD)));
		
		SearchResponse searchRes = searchReq.execute().actionGet();
		Terms groupByInstanceUniqueIds = searchRes.getAggregations().get(GROUP_BY_INSTANCE_UNIQUE_ID);
		
		List<String> instanceUniqueIdList = new ArrayList<>();
		if(groupByInstanceUniqueIds == null)
		{
			return instanceUniqueIdList;
		}
		
		for(Terms.Bucket filedBucket : groupByInstanceUniqueIds.getBuckets())
		{
			instanceUniqueIdList.add(filedBucket.getKeyAsString());
		}
		return instanceUniqueIdList;
	}
	
	private SearchRequestBuilder getSolutionhHistoryQueryBuilder()
	{
		SearchRequestBuilder searchReqBuilder = esConfiguration.client().prepareSearch(environmentProperties.getElasticClusterName());
		searchReqBuilder.setTypes(environmentProperties.getElasticClusterTypeSolutionHistory());
		return searchReqBuilder;
	}
}
