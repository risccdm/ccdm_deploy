package com.ccdm.deploy.provider.validator;

import org.springframework.stereotype.Component;

import com.ccdm.deploy.common.validator.CommonValidator;
import com.ccdm.deploy.elastic.solutionhistory.model.AgentClientRequestVo;
import com.ccdm.deploy.elastic.solutionhistory.model.AgentMasterConfigRequestVo;
import com.ccdm.deploy.elastic.solutionhistory.model.ClientInstanceVo;
import com.ccdm.deploy.elastic.solutionhistory.model.CreateInstanceRequestVo;
import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.provider.constants.EProvider;
import com.ccdm.deploy.solution.model.ServerCredentialVo;
import com.ccdm.deploy.vo.ServerCredential;
import com.ccdm.deploy.vo.ViewModel;

@Component
public class InstanceValidator 
{
	public static void validateCreateInstaceRequestVo(CreateInstanceRequestVo request) throws CCDMException
	{
		CommonValidator.validateObject(request.getInstanceConfig(), ErrorCodes.INVALID_INSTANCE_CONFIG);
		
		CommonValidator.validateString(request.getInstanceConfig().getImageId(), ErrorCodes.INVALID_IMAGE_ID);
		CommonValidator.validateString(request.getInstanceConfig().getInstanceType(), ErrorCodes.INVALID_INSTANCE_TYPE);
		CommonValidator.validateString(request.getInstanceConfig().getPemKey(), ErrorCodes.INVALID_KEY_NAME);
		CommonValidator.validateString(request.getInstanceName(), ErrorCodes.INVALID_TAG_NAME);
		CommonValidator.validateString(request.getInstanceConfig().getSecurityGroup(), ErrorCodes.SECURITY_GROUP_EMPTY);
		CommonValidator.validateString(request.getCreatedFor(), ErrorCodes.CREATED_FOR_NAME_EMPTY);
	}

	public void validateViewModel(ViewModel viewModel) throws CCDMException 
	{
		CommonValidator.validateObject(viewModel, ErrorCodes.INVALID_SOLUTION_REQUEST);
	}

	public static void validateServerCredential(ServerCredential serverCredential) throws CCDMException 
	{
		CommonValidator.validateObject(serverCredential, ErrorCodes.INVALID_SERVER_CREDENTIAL_OBJECT);
		CommonValidator.validateString(serverCredential.getAccessId(), ErrorCodes.INVALID_ACCESS_ID);
		CommonValidator.validateString(serverCredential.getAccessKey(), ErrorCodes.INVALID_ACCESS_KEY);
		CommonValidator.validateString(serverCredential.getPemKey(), ErrorCodes.INVALID_PRIVATE_KEY);
		CommonValidator.validateString(serverCredential.getUser(), ErrorCodes.INVALID_CREDENTIAL_USER_NAME);
	}

	public static void validateInstance(ClientInstanceVo instanceInfoVo) throws CCDMException 
	{
		CommonValidator.validateObject(instanceInfoVo, ErrorCodes.INVALID_INSTANCE);
		CommonValidator.validateString(instanceInfoVo.getInstanceName(), ErrorCodes.INVALID_INSTANCE_TAG_NAME);
		CommonValidator.validateString(instanceInfoVo.getInstanceId(), ErrorCodes.INVALID_INSTANCE_ID);
		CommonValidator.validateString(instanceInfoVo.getKeyName(), ErrorCodes.INVALID_INSTANCE_KEY);
		CommonValidator.validateString(instanceInfoVo.getPublicDns(), ErrorCodes.INVALID_INSTANCE_PUBLIC_DNS);
		CommonValidator.validateString(instanceInfoVo.getPublicIpv4(), ErrorCodes.INVALID_INSTANCE_PUBLIC_IP4);
	}

	public static void validateServerCredentialVo(ServerCredentialVo serverCredentialVo) throws CCDMException 
	{
		CommonValidator.validateString(serverCredentialVo.getUser(), ErrorCodes.INVALID_CREDENTIAL_USER_NAME);
		if(EProvider.AMAZON.getProviderName().equals(serverCredentialVo.getProviderName())) {
			CommonValidator.validateString(serverCredentialVo.getPemKey(), ErrorCodes.INVALID_PRIVATE_KEY);
			CommonValidator.validateObject(serverCredentialVo, ErrorCodes.INVALID_SERVER_CREDENTIAL_OBJECT);
			CommonValidator.validateString(serverCredentialVo.getAccessId(), ErrorCodes.INVALID_ACCESS_ID);
			CommonValidator.validateString(serverCredentialVo.getAccessKey(), ErrorCodes.INVALID_ACCESS_KEY);
		}
	}

	public static void validateMasterAgentRequest(AgentMasterConfigRequestVo masterAgentConfigRequest) throws CCDMException 
	{
		CommonValidator.validateObject(masterAgentConfigRequest, ErrorCodes.INVALID_MASTER_AGENT_REQ);
		validateInstance(masterAgentConfigRequest.getClientInstanceVo());
		validateInstance(masterAgentConfigRequest.getMasterInstanceVo());
	}

	public static void validateClientAgentRequest(AgentClientRequestVo clientAgentRequest) throws CCDMException 
	{
		CommonValidator.validateObject(clientAgentRequest, ErrorCodes.INVALID_CLIENT_AGENT_REQ);
		validateInstance(clientAgentRequest.getClientInstanceVo());
		CommonValidator.validateString(clientAgentRequest.getTicketSalt(), ErrorCodes.INVALID_TICKET_SALT);
		CommonValidator.validateString(clientAgentRequest.getUploadFilePath(), ErrorCodes.INVALID_UPLOAD_FILE_PATH);
	}
}
