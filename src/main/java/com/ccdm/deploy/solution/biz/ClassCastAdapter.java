package com.ccdm.deploy.solution.biz;

import org.springframework.stereotype.Component;

import com.ccdm.deploy.elastic.vo.SolutionHistory;
import com.ccdm.deploy.exceptions.CCDMException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class ClassCastAdapter 
{
	public Object convertRequestToRealObject(SolutionHistory solutionHistory, String task, Class<?> clazz) throws CCDMException 
	{
		return new ObjectMapper().convertValue(solutionHistory.getRequestObject(), clazz);
	}

	public Object convertResponseToRealObject(SolutionHistory solutionHistory, String instanceTaskName, Class<?> clazz) 
	{
		return new ObjectMapper().convertValue(solutionHistory.getResponseObject(), clazz);
	}
}
