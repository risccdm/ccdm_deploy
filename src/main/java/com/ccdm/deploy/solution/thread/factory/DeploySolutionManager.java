package com.ccdm.deploy.solution.thread.factory;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.elastic.repository.solutionhistory.ISolutionHistoryRepoCustom;
import com.ccdm.deploy.elastic.repository.solutionhistory.ISolutionHistoryRepository;
import com.ccdm.deploy.elastic.vo.SolutionHistory;
import com.ccdm.deploy.solution.constants.ESolutionState;
import com.ccdm.deploy.webServer.provider.task.utils.ProviderUtils;

@Component
public class DeploySolutionManager
{
	private static Logger logger = LoggerFactory.getLogger(DeploySolutionManager.class);
	
	@Autowired
	private ISolutionHistoryRepoCustom solutionHistoryRepoCustom;
	@Autowired
	private ISolutionHistoryRepository solutionHistoryRepo;
	@Autowired
	private DeploySolutionInitiator deploySolutionInitiator;
	@Autowired
	private ProviderUtils providerUtils;
	@Autowired
	ThreadPoolTaskExecutor taskExecutor;
	
	private static final int THREAD_SLEEP_TIME_OUT = 5000;
	
	public void manageProviderTask()
	{
		new Thread(new Runnable() 
		{
			@Override
			public void run() 
			{
				try 
				{
					Thread.currentThread().setName("Deploy_Manager");
					logger.info(" <========== Deploy Solution Thread Manager will start within 10 sec ============>");
					Thread.sleep(THREAD_SLEEP_TIME_OUT * 2);
					logger.info("Deploy Solution Thread Manager Started...!");
				} 
				catch (InterruptedException e1) {
					logger.error(e1.getMessage());
				}
				
				while(true)
				{
					try
					{
						List<String> instanceUniqueIdList = solutionHistoryRepoCustom.getInstanceUniqueIdsBySolutionStateAndOrder(ESolutionState.WAITING.getSolutionState(), true);
						
						if(instanceUniqueIdList != null && instanceUniqueIdList.size() > 0) 
						{
							logger.info("Total Solution in Waiting State "+instanceUniqueIdList.size());
							/* get Latest Solution By First InstanceUniqueId to Deploy */
							String instanceUniqueId  = instanceUniqueIdList.get(0);
							List<SolutionHistory> solutionHistoryList = solutionHistoryRepo.findAllByInstanceUniqueIdOrderBySeqNoAsc(instanceUniqueId);
							
							String logFileName = instanceUniqueId;
							if(solutionHistoryList != null && solutionHistoryList.size() > 0)
							{
								logFileName = solutionHistoryList.get(0).getSolutionName()+"_"+solutionHistoryList.get(0).getCreatedFor()+"_"+solutionHistoryList.get(0).getInstanceName();
								/* Update Status Of SolutionHistory from Waiting to Initiated */
								for(SolutionHistory solHistory : solutionHistoryList)
								{
									/* update waiting state entry only., because in group by query it will return all list if any one matches group by condition */
									if(solHistory.getStatus().equalsIgnoreCase(ESolutionState.WAITING.getSolutionState()))
									{
										providerUtils.updateSolutionHistoryStatus(solHistory, ESolutionState.INITIALIZED.getSolutionState());
									}
								}
								
								/* Call Child Thread to start Deploy Process */
								deploySolutionInitiator.initiateDeploySolutionThread(instanceUniqueId, logFileName);
							}
							
							// Next Process will start within 10 sec
							gotoSleep(THREAD_SLEEP_TIME_OUT * 2);
						}
						else
						{
							//logger.info("No Solution in Waiting State.. Main Thread Going to Sleep for 30 sec");
							gotoSleep(THREAD_SLEEP_TIME_OUT * 6);
						}
					}
					catch(Exception e) 
					{
						logger.error("<--------------- Exception in Main Thread. -------------->");
						logger.error(e.getMessage());
						logger.info("Main Thread going to Shutting Down.");
						
						//check active thread, if zero then shut down the thread pool
						for (;;) 
						{
							int count = taskExecutor.getActiveCount();
							logger.info("Active Threads : " + count);
							try 
							{
								logger.info("Main Thread Will wait until thread pool empty...");
								Thread.sleep(THREAD_SLEEP_TIME_OUT);
							} 
							catch (InterruptedException ex) 
							{
								logger.error(ex.getMessage());
							}
							if (count == 0) 
							{
								logger.info("Main thread shutting down . . .");
								taskExecutor.shutdown();
								break;
							}
						}
						logger.info("Main Thread has been shutdown.");
						break;
					}
				}
			}

			private void gotoSleep(int threadSleepTimeOut) throws InterruptedException 
			{
				Thread.sleep(threadSleepTimeOut);
			}
		}).start();
	}
}
