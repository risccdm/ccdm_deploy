package com.ccdm.deploy.solution.thread.factory;

import java.util.List;

import org.apache.log4j.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.elastic.repository.solutionhistory.ISolutionHistoryRepository;
import com.ccdm.deploy.elastic.vo.SolutionHistory;
import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.solution.constants.ESolutionState;
import com.ccdm.deploy.webServer.provider.ProviderTaskExecutor;

@Component
public class DeploySolutionInitiator
{
	private static Logger logger = LoggerFactory.getLogger(DeploySolutionInitiator.class);

	@Autowired
	private ISolutionHistoryRepository solutionHistoryRepo;
	@Autowired
	private ProviderTaskExecutor providerTaskExecutor;
	
	@Async("deployTaskExecutor")
	public void initiateDeploySolutionThread(String instanceUniqueId, String logFileName) throws CCDMException
	{
		MDC.put("logFileName", logFileName);
		try
		{
			List<SolutionHistory> solutionHistoryList = solutionHistoryRepo
					.findAllByInstanceUniqueIdAndStatusOrderBySeqNoAsc(instanceUniqueId, ESolutionState.INITIALIZED.getSolutionState());
			
			if(solutionHistoryList == null || solutionHistoryList.size() == 0)
			{
				logger.info("No Solution Available to Deploy.!");
			}
			else
			{
				for(SolutionHistory solutionHistory : solutionHistoryList)
				{
					Thread.currentThread().setName(solutionHistory.getInstanceName()+"_Task_"+solutionHistory.getSeqNo());
					logger.info("Deploy Started for Solution(deployUniqueId => "+solutionHistory.getDeployUniqueId()+") : '"+solutionHistory.getSolutionName()
								+"' and for Customer : '"+solutionHistory.getCreatedFor()+"'");
					
					providerTaskExecutor.executeTaskByProvider(solutionHistory.getId());
				}
			}
		}
		catch(Exception e) {
			logger.info("Cannot Deploy Solution. Reason : "+e.getMessage());
			throw new CCDMException(ErrorCodes.INTERNAL_ERROR+" ----> "+e.getMessage());
		}
		finally {
			MDC.remove("logFileName");
		}
	}
}
