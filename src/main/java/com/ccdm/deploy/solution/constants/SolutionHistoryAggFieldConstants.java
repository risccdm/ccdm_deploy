package com.ccdm.deploy.solution.constants;

public class SolutionHistoryAggFieldConstants 
{
	public static final String STATUS_FIELD 				= "status";
	public static final String INSTANCE_UNIQUE_ID_FIELD 	= "instanceUniqueId";
	public static final String DEPLOY_DATE_FIELD 			= "deployDate";
	
	public static final String GROUP_BY_TERM 	= "group_by_";
	public static final String MAX_TERM 		= "max_";
}
