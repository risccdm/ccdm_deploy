package com.ccdm.deploy.agent.model;

import com.ccdm.deploy.elastic.solutionhistory.model.ClientInstanceVo;
import com.ccdm.deploy.solution.model.ServerCredentialVo;

public class ExecutionScriptRequest 
{
	private ClientInstanceVo instanceVo;
	private ServerCredentialVo serverCredentialVo;
	private String script;
	
	public String getScript() {
		return script;
	}
	public void setScript(String script) {
		this.script = script;
	}
	public ServerCredentialVo getServerCredentialVo() {
		return serverCredentialVo;
	}
	public void setServerCredentialVo(ServerCredentialVo serverCredentialVo) {
		this.serverCredentialVo = serverCredentialVo;
	}
	public ClientInstanceVo getInstanceVo() {
		return instanceVo;
	}
	public void setInstanceVo(ClientInstanceVo instanceVo) {
		this.instanceVo = instanceVo;
	}
}
