package com.ccdm.deploy.agent.enums;

public enum ScripExecutionStatus 
{
	SUCCESS("Success"), FAILURE("Failure");
	
	private String scriptLogStatus;
	
	private ScripExecutionStatus(String scriptLogStatus) {
		this.scriptLogStatus = scriptLogStatus;
	}
	
	public String getScriptLogStatus() {
		return this.scriptLogStatus;
	}
}
