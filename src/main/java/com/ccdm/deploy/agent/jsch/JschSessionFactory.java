package com.ccdm.deploy.agent.jsch;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.elastic.solutionhistory.model.ClientInstanceVo;
import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.instance.constants.InstanceConstant;
import com.ccdm.deploy.solution.model.ServerCredentialVo;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

@Component
public class JschSessionFactory 
{
	private static final Logger logger = LoggerFactory.getLogger(JschSessionFactory.class);
	private static String PEM_KEY_FILE_PATH = InstanceConstant.PEM_FILE_PATH;
	
	public Session getJschSession(ServerCredentialVo credentialVo, ClientInstanceVo instanceVo) throws CCDMException
	{
		Session session = null;
		JSch jsch = new JSch();
		try
		{
			if(instanceVo.getKeyName() != null)
			{
				jsch.setKnownHosts(PEM_KEY_FILE_PATH + instanceVo.getKeyName()+".pem");
				jsch.addIdentity(PEM_KEY_FILE_PATH + instanceVo.getKeyName()+".pem");
				logger.info("Identity Added with pem file : "+instanceVo.getKeyName()+".pem");
			}
			
			if(instanceVo.getPort()!= null && instanceVo.getPort() > 0)
			{
				session = jsch.getSession(credentialVo.getUser(), instanceVo.getPublicIpv4(), instanceVo.getPort());
			}
			else
			{
				session = jsch.getSession(credentialVo.getUser(), instanceVo.getPublicIpv4());	// By Default TCP port 22 will be used in making the connection.
			}

			// disabling StrictHostKeyChecking may help to make connection but makes it insecure
			Properties config = new Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			
			logger.info("connecting to session...");
			connectToSession(1, session);
			return session;
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
			throw new CCDMException(ErrorCodes.CANNOT_CONNECT_TO_SERVER);
		}
	}
	
	private void connectToSession(Integer noOfTry, Session session) throws CCDMException, InterruptedException 
	{
		try
		{
			if(noOfTry <= 5)
			{
				if(!session.isConnected())
				{
					logger.info("Connecting to Session.. try : "+noOfTry);
					session.connect(25000);
				}
			}
			else
			{
				throw new CCDMException(ErrorCodes.CANNOT_CONNECT_TO_SERVER);
			}
			noOfTry++;
			if(session.isConnected())
			{
				logger.info("Session Connected..");
				return;
			}
		}
		catch(Exception e)
		{
			noOfTry++;
			if(noOfTry > 5) {
				throw new CCDMException(ErrorCodes.CANNOT_CONNECT_TO_SERVER);
			}
			logger.info("unable to connect session in try : "+noOfTry+" REASON : "+e.getMessage());
			Thread.sleep(2000);
			connectToSession(noOfTry, session);
		}
	}
}
