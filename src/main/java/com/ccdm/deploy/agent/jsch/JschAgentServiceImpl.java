package com.ccdm.deploy.agent.jsch;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.agent.enums.ScripExecutionStatus;
import com.ccdm.deploy.agent.model.ExecutionScriptRequest;
import com.ccdm.deploy.agent.model.ExecutionScriptResponse;
import com.ccdm.deploy.agent.service.AgentService;
import com.ccdm.deploy.agent.validator.AgentValidator;
import com.ccdm.deploy.elastic.solutionhistory.model.AgentClientRequestVo;
import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;

@Component
public class JschAgentServiceImpl implements AgentService
{
	private static final Logger logger = LoggerFactory.getLogger(JschAgentServiceImpl.class);
	
	@Autowired
	private JschExec jschExec;
	
	@Override
	public ExecutionScriptResponse executeScript(ExecutionScriptRequest executionScriptRequest) throws CCDMException 
	{
		AgentValidator.validateExecutionScript(executionScriptRequest);
		ExecutionScriptResponse response = new ExecutionScriptResponse();
		
		// TODO Validate executionScriptRequest
		logger.info("executing script in an instance : "+executionScriptRequest.getInstanceVo().getInstanceName());
		response.setExecutedDate(new Date());
		String output = jschExec.execute(executionScriptRequest);
		logger.info("script executed in an instance : "+executionScriptRequest.getInstanceVo().getInstanceName()+" on "+response.getExecutedDate());
		
		// Saving output
		response.setInstanceId(executionScriptRequest.getInstanceVo().getInstanceId());
		response.setScriptOutput(output);
		response.setStatus(ScripExecutionStatus.SUCCESS);
		return response;
	}
	
	public void uploadFile(AgentClientRequestVo clientAgentRequest) throws CCDMException
	{
		logger.info("Uploading "+clientAgentRequest.getUploadFilePath()+" File to Instance Using sftp.");
		jschExec.uploadFile(clientAgentRequest);
		logger.info("File Uploaded Successfully...");
	}

	public String configAgent(ExecutionScriptRequest executionScriptRequest) throws CCDMException 
	{
		try
		{
			AgentValidator.validateExecutionScript(executionScriptRequest);
			String ticketSalt = jschExec.execute(executionScriptRequest);
			return ticketSalt;
		}
		catch(Exception e) {
			logger.error(e.getMessage());
			throw new CCDMException(ErrorCodes.ERROR_CLIENT_CONFIG_IN_MASTER_ICINGA);
		}
	}
	
	public String installAgent(ExecutionScriptRequest executionScriptRequest) throws CCDMException 
	{
		try
		{
			AgentValidator.validateExecutionScript(executionScriptRequest);
			logger.info("Client Agent Installation will start within 20 sec..");
			Thread.sleep(20000);
			String output = jschExec.execute(executionScriptRequest);
			return output;
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
			throw new CCDMException(ErrorCodes.ERROR_CLIENT_ICINGA_CONFIG);
		}
	}
}
