package com.ccdm.deploy.agent.jsch;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.agent.model.ExecutionScriptRequest;
import com.ccdm.deploy.elastic.solutionhistory.model.AgentClientRequestVo;
import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

@Component
public class JschExec 
{
	private static final Logger logger = LoggerFactory.getLogger(JschExec.class);
	
	/*@Autowired
	private JschSessionFactory jschSessionFactory;*/
	@Autowired
	private JschChannelFactory jschChannelFactory;
	
	/*private Session session = null;
	private Channel channel = null;*/

	public String execute(ExecutionScriptRequest executionScriptRequest) throws CCDMException 
	{
		Session session = null;
		Channel channel = null;
		
		String scriptOuput = "";
		try
		{
			session = new JschSessionFactory().getJschSession(executionScriptRequest.getServerCredentialVo(), executionScriptRequest.getInstanceVo());
			channel = jschChannelFactory.getExecChannel(session);
			
			((ChannelExec) channel).setCommand(executionScriptRequest.getScript());
			
			scriptOuput = logChannelOutput(session, channel);
		}
		catch(JSchException ex)
		{
			closingConnection(session, channel);
			logger.error(ex.getMessage());
			throw new CCDMException(ErrorCodes.CANNOT_OPEN_CHANNEL);
		}
		catch(Exception e)
		{
			closingConnection(session, channel);
			logger.error(e.getMessage());
			throw new CCDMException(ErrorCodes.JSCH_INTERNAL_ERROR);
		}
		return scriptOuput;
	}
	
	public void uploadFile(AgentClientRequestVo clientAgentRequest) throws CCDMException 
	{
		Session session = null;
		Channel channel = null;
		try
		{
			session = new JschSessionFactory().getJschSession(clientAgentRequest.getServerCredentialVo(), clientAgentRequest.getClientInstanceVo());
			channel = jschChannelFactory.getSftpChannel(session);
			
			logger.info("Connecting to Channel for Upload File.....");
			connectToChannel(1, channel);
			
			ChannelSftp channelSftp = (ChannelSftp) channel;
			File f1 = new File(clientAgentRequest.getUploadFilePath());
			channelSftp.put(new FileInputStream(f1), f1.getName());
			logger.info("File Uploaded Successfully..");
			
			channelSftp.exit();
			channelSftp.disconnect();
			logger.info("sftp channel Closed...");
			closingConnection(session, channel);
		}
		catch(JSchException ex)
		{
			closingConnection(session, channel);
			logger.error(ex.getMessage());
			throw new CCDMException(ErrorCodes.CANNOT_UPLOAD_FILE);
		}
		catch(Exception e)
		{
			closingConnection(session, channel);
			logger.error(e.getMessage());
			throw new CCDMException(ErrorCodes.JSCH_INTERNAL_ERROR);
		}
	}
	
	private void connectToChannel(Integer noOfTry, Channel channel) throws CCDMException 
	{
		try
		{
			if(noOfTry <= 5)
			{
				if(!channel.isConnected())
				{
					logger.info("Connecting to Channel.. try : "+noOfTry);
					channel.connect(25000);
				}
			}
			else
			{
				throw new CCDMException(ErrorCodes.CANNOT_OPEN_CHANNEL);
			}
			noOfTry++;
			if(channel.isConnected())
			{
				logger.info("Channel Connected..");
				return;
			}
		}
		catch(Exception e)
		{
			logger.error("unable to connect in try -> "+noOfTry+" REASON : "+e.getMessage());
			noOfTry++;
			if(noOfTry > 5) {
				throw new CCDMException(ErrorCodes.CANNOT_OPEN_CHANNEL);
			}
			logger.info("trying to connect channel again with in 3 min... try -> "+noOfTry);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {}
			connectToChannel(noOfTry, channel);
		}
	}

	private String logChannelOutput(Session session, Channel channel) throws CCDMException, IOException, JSchException 
	{
		String scriptOutput = "";
		try
		{
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);
		
			InputStream in  = channel.getInputStream();
			logger.info("Connecting Log Channel...");
			connectToChannel(1, channel);
			
			StringBuffer strBuff = new StringBuffer();
			byte[] tmp = new byte[1024];
			
			long initialTimeout = Calendar.getInstance().getTimeInMillis();
			long timeDiff = 0;
			long maxTimeOut = 30000;
			
			while(true)
			{
				if(timeDiff > maxTimeOut)
				{
					logger.info("Logging too long ------------> exit from Log reader..!");
					Thread.sleep(3000);
					break;
				}
				
				while(in.available() > 0)
				{
					int i = in.read(tmp, 0, 1024);
					if(i < 0) break;
					//System.out.println(new String(tmp, 0, i));
					String string = new String(tmp, 0, i)+System.lineSeparator();
					logger.info(string);
					strBuff.append(string);
					break;
				}
				
				if(channel.isClosed())
				{
					logger.info("exit-status : "+channel.getExitStatus());
					break;
				}
				
				long currentTimeout = Calendar.getInstance().getTimeInMillis();
				timeDiff = (currentTimeout - initialTimeout);
				
				try
				{
					// to get all logs - commend below thread sleep
					//Thread.sleep(60000);
				}
				catch(Exception e) { }
			}
			scriptOutput = strBuff.toString();
			closingConnection(session, channel);
		}
		catch(Exception ex)
		{
			logger.error(ex.getMessage());
			throw new CCDMException(ErrorCodes.CANNOT_LOG_CHANNEL_OUTPUT); 
		}
		return scriptOutput;
	}
	
	private void closingConnection(Session session, Channel channel) 
	{
		logger.info("DONE -- Disconnecting...");
		if(!channel.isClosed())
		{
			channel.disconnect();
			logger.info("channel disconnected...");
		}
		if(session.isConnected())
		{
			session.disconnect();
			logger.info("session disconnection...");
		}
	}
}
