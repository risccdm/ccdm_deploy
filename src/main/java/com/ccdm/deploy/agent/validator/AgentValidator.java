package com.ccdm.deploy.agent.validator;

import com.ccdm.deploy.agent.model.ExecutionScriptRequest;
import com.ccdm.deploy.common.validator.CommonValidator;
import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;

public class AgentValidator 
{
	public static void validateExecutionScript(ExecutionScriptRequest executionScriptRequest) throws CCDMException 
	{
		CommonValidator.validateObject(executionScriptRequest, ErrorCodes.EXECUTION_SCRIPT_EMPTY);
		CommonValidator.validateString(executionScriptRequest.getScript(), ErrorCodes.INVALID_SCRIPT);
	}
}
