package com.ccdm.deploy.agent.service;

import com.ccdm.deploy.agent.model.ExecutionScriptRequest;
import com.ccdm.deploy.agent.model.ExecutionScriptResponse;
import com.ccdm.deploy.elastic.solutionhistory.model.AgentClientRequestVo;
import com.ccdm.deploy.exceptions.CCDMException;

public interface AgentService 
{
	ExecutionScriptResponse executeScript(ExecutionScriptRequest executionScriptRequest) throws CCDMException;
	
	void uploadFile(AgentClientRequestVo clientAgentRequest) throws CCDMException;
	
	String configAgent(ExecutionScriptRequest executionScriptRequest) throws CCDMException;
	
	String installAgent(ExecutionScriptRequest executionScriptRequest) throws CCDMException;
}
