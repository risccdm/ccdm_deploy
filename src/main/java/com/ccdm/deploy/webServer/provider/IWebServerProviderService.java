package com.ccdm.deploy.webServer.provider;

public interface IWebServerProviderService 
{
	void executeTaskByProvider(String solutionHistoryId);
}
