package com.ccdm.deploy.webServer.provider.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.elastic.solutionhistory.model.AgentMasterConfigRequestVo;
import com.ccdm.deploy.elastic.solutionhistory.model.AgentMasterConfigResponseVo;
import com.ccdm.deploy.elastic.vo.SolutionHistory;
import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.instance.constants.EInstanceTask;
import com.ccdm.deploy.instance.controller.InstanceController;
import com.ccdm.deploy.solution.biz.ClassCastAdapter;
import com.ccdm.deploy.solution.constants.ESolutionState;
import com.ccdm.deploy.webServer.provider.task.utils.ProviderUtils;

@Component("masterAgentConfigurationTask")
public class MasterAgentConfigurationTask implements IWebServerProviderTask 
{
	private static Logger logger = LoggerFactory.getLogger(MasterAgentConfigurationTask.class);
	
	@Autowired
	private ClassCastAdapter classCastAdapter;
	@Autowired
	private InstanceController instanceController;
	@Autowired
	private ProviderUtils taskUtils;
	
	@Override
	public void doTask(SolutionHistory solutionHistory) throws CCDMException
	{
		try
		{
			// step-0 : check Dependecy task has been finished or not by refId
			logger.info("Task-2 : '"+EInstanceTask.MASTER_AGENT_CONFIG+"'...");
			logger.info("Checking Dependecy Task for "+EInstanceTask.MASTER_AGENT_CONFIG.getTaskName());
			boolean isDependecyTaskFinished = taskUtils.isDependecyInstanceTaskCompleted(solutionHistory);
			
			if(isDependecyTaskFinished)
			{
				// step-1 Update INPROGRESS STATUS
				taskUtils.updateSolutionHistoryStatus(solutionHistory, ESolutionState.INPROGRESS.getSolutionState());
				logger.info("*************** "+ESolutionState.INPROGRESS.name()+" ***************");
				
				// step-2 call to Config Icinga Master
				AgentMasterConfigRequestVo masterAgentConfigRequest = (AgentMasterConfigRequestVo) classCastAdapter.convertRequestToRealObject(solutionHistory, EInstanceTask.MASTER_AGENT_CONFIG.getTaskName(), AgentMasterConfigRequestVo.class);
				logger.info("Config Master Agent Initiated...");
				AgentMasterConfigResponseVo agentMasterConfigResponseVo = masterAgentConfiguration(masterAgentConfigRequest);
				
				// step-3 saving response to solution history
				solutionHistory.setResponseObject(agentMasterConfigResponseVo);
				taskUtils.saveOrUpdateSolutionHistory(solutionHistory);
				logger.info("response saved...!");
				
				// step-4 : Updating Instance Details to Requestion object of upcoming same Instance Tasks by Instance's common id
				taskUtils.updateTicketSaltToClientAgent(solutionHistory, agentMasterConfigResponseVo.getTicketSalt());
				logger.info("Client Instance Request Object Detail Updated after Master Agent Configured.");
				
				// step-5 : update COMPLETED STATUS
				taskUtils.updateSolutionHistoryStatus(solutionHistory, ESolutionState.COMPLETED.getSolutionState());
				logger.info("*************** "+ESolutionState.COMPLETED.name()+" ***************");
			}
			else 
			{
				taskUtils.updateSolutionHistoryStatus(solutionHistory, ESolutionState.SKIPPED.getSolutionState());
				logger.info(EInstanceTask.MASTER_AGENT_CONFIG.getTaskName()+" Task cannot be start and Skipped. since dependecy task not Completed");
			}
		}
		catch(Exception e) 
		{
			taskUtils.updateSolutionHistoryStatus(solutionHistory, ESolutionState.FAILED.getSolutionState());
			throw new CCDMException(ErrorCodes.MASTER_AGENT_CONFIG_FAILED);
		}
	}
	
	/* *********************************** Private Methods ********************************* */
	private AgentMasterConfigResponseVo masterAgentConfiguration(AgentMasterConfigRequestVo masterAgentConfigRequest) throws CCDMException 
	{
		// 1a. config master agent and get ticket salt
		String ticketSalt = instanceController.configIcingaMaster(masterAgentConfigRequest);
		
		AgentMasterConfigResponseVo agentMasterConfigResponseVo = new AgentMasterConfigResponseVo();
		agentMasterConfigResponseVo.setTicketSalt(ticketSalt);
		return agentMasterConfigResponseVo;
	}
}