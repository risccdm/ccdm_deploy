package com.ccdm.deploy.webServer.provider.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.elastic.solutionhistory.model.CreateInstanceRequestVo;
import com.ccdm.deploy.elastic.vo.SolutionHistory;
import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.instance.constants.EInstanceTask;
import com.ccdm.deploy.instance.controller.InstanceController;
import com.ccdm.deploy.solution.biz.ClassCastAdapter;
import com.ccdm.deploy.solution.constants.ESolutionState;
import com.ccdm.deploy.webServer.provider.task.utils.ProviderUtils;
import com.ccdm.deploy.webprovider.response.InstanceCreateResponse;

@Component("createInstanceTask")
public class CreateInstanceTask implements IWebServerProviderTask 
{
	private static Logger logger = LoggerFactory.getLogger(CreateInstanceTask.class);
	
	@Autowired
	private ClassCastAdapter classCastAdapter;
	@Autowired
	private InstanceController instanceController;
	@Autowired
	private ProviderUtils taskUtils;
	
	@Override
	public void doTask(SolutionHistory solutionHistory) throws CCDMException 
	{
		try
		{
			// step-1 Update INPROGRESS STATUS
			logger.info("Task-1 : '"+EInstanceTask.CREATE_INSTANCE+"'...");
			taskUtils.updateSolutionHistoryStatus(solutionHistory, ESolutionState.INPROGRESS.getSolutionState());
			logger.info("*************** "+ESolutionState.INPROGRESS.name()+" ***************");
			
			// step-2 : Call to Create Instance Process Method
			CreateInstanceRequestVo creatInstanceRequestVo = (CreateInstanceRequestVo) classCastAdapter.convertRequestToRealObject(solutionHistory, EInstanceTask.CREATE_INSTANCE.getTaskName(), CreateInstanceRequestVo.class);
			logger.info("Create Instance Initiated...");
			InstanceCreateResponse instanceCreateResponse = instanceController.createInstance(creatInstanceRequestVo);
			
			// step-3 : Saving Response to Solution History
			solutionHistory.setResponseObject(instanceCreateResponse);
			taskUtils.saveOrUpdateSolutionHistory(solutionHistory);
			logger.info("CreateInstanceResponse saved...!");
			
			// step-4 : Updating Instance Details to Requestion object of upcoming same Instance Tasks by Instance's common id
			logger.info("Updating Request Object of Upcoming Tasks");
			taskUtils.updatedCreatedInstanceDetails(solutionHistory);
			
			// step-5 : updating COMPLETED STATUS
			taskUtils.updateSolutionHistoryStatus(solutionHistory, ESolutionState.COMPLETED.getSolutionState());
			logger.info("*************** "+ESolutionState.COMPLETED.name()+" ***************");
			logger.info("Instance  Created");
			
		}
		catch(Exception e) 
		{
			taskUtils.updateSolutionHistoryStatus(solutionHistory, ESolutionState.FAILED.getSolutionState());
			throw new CCDMException(ErrorCodes.CREATE_INSTANCE_TASK_FAILED);
		}
	}
}