package com.ccdm.deploy.webServer.provider.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.elastic.solutionhistory.model.AgentClientRequestVo;
import com.ccdm.deploy.elastic.solutionhistory.model.AgentClientResponseVo;
import com.ccdm.deploy.elastic.vo.SolutionHistory;
import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.instance.constants.EInstanceTask;
import com.ccdm.deploy.instance.controller.InstanceController;
import com.ccdm.deploy.solution.biz.ClassCastAdapter;
import com.ccdm.deploy.solution.constants.ESolutionState;
import com.ccdm.deploy.webServer.provider.task.utils.ProviderUtils;

@Component("clientAgentInstallationTask")
public class ClientAgentInstallationTask implements IWebServerProviderTask 
{
	private static Logger logger = LoggerFactory.getLogger(ClientAgentInstallationTask.class);
	
	@Autowired
	private ClassCastAdapter classCastAdapter;
	@Autowired
	private InstanceController instanceController;
	@Autowired
	private ProviderUtils taskUtils;
	
	@Override
	public void doTask(SolutionHistory solutionHistory) throws CCDMException
	{
		try
		{
			// step-0 : check Dependecy task has been finished or not by refId
			logger.info("Task-3 : '"+EInstanceTask.CLIENT_AGENT_INSTALL+"'...");
			logger.info("Checking Dependecy Task for "+EInstanceTask.CLIENT_AGENT_INSTALL.getTaskName());
			boolean isDependecyTaskFinished = taskUtils.isDependecyInstanceTaskCompleted(solutionHistory);
			
			if(isDependecyTaskFinished)
			{
				// step-1 Update INPROGRESS STATUS
				taskUtils.updateSolutionHistoryStatus(solutionHistory, ESolutionState.INPROGRESS.getSolutionState());
				logger.info("*************** "+ESolutionState.INPROGRESS.name()+" ***************");
				
				// step-2 : call to install icinga-client with help of icinga-client req 
				AgentClientRequestVo clientAgentRequest = (AgentClientRequestVo) classCastAdapter.convertRequestToRealObject(solutionHistory, EInstanceTask.CLIENT_AGENT_INSTALL.getTaskName(), AgentClientRequestVo.class);
				logger.info("Installing Client Agent Initiated...");
				AgentClientResponseVo clientAgentResponse = clientAgentInstallation(clientAgentRequest);
				
				// step-3 : saving response to solution history
				solutionHistory.setResponseObject(clientAgentResponse);
				taskUtils.saveOrUpdateSolutionHistory(solutionHistory);
				logger.info("response saved...!");
				
				// step-4 : update COMPLETED STATUS
				taskUtils.updateSolutionHistoryStatus(solutionHistory, ESolutionState.COMPLETED.getSolutionState());
				logger.info("*************** "+ESolutionState.COMPLETED.name()+" ***************");
			}
			else 
			{
				taskUtils.updateSolutionHistoryStatus(solutionHistory, ESolutionState.SKIPPED.getSolutionState());
				logger.info(EInstanceTask.CLIENT_AGENT_INSTALL.getTaskName()+" Task cannot be start. since dependecy task not Completed");
			}
		}
		catch(Exception  e) 
		{
			taskUtils.updateSolutionHistoryStatus(solutionHistory, ESolutionState.FAILED.getSolutionState());
			throw new CCDMException(ErrorCodes.CLIENT_AGENT_CONFIG_FAILED);
		}
	}
	
	/* *********************************** Private Methods ********************************* */
	private AgentClientResponseVo clientAgentInstallation(AgentClientRequestVo clientAgentRequest) throws CCDMException, InterruptedException 
	{
		taskUtils.sleepThreadByTimeOut("Sleeping 60 sec for Client Agent come to Accessable state.....", 60000);
		
		// 1a. upload file & install icinga client with ticket salt
		String installedOutput = instanceController.installIcingaClient(clientAgentRequest);
		
		AgentClientResponseVo agentClientResponseVo = new AgentClientResponseVo();
		agentClientResponseVo.setOutput(installedOutput);
		return agentClientResponseVo;
	}
}