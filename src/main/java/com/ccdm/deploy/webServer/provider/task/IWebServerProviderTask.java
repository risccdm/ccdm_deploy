package com.ccdm.deploy.webServer.provider.task;

import com.ccdm.deploy.elastic.vo.SolutionHistory;
import com.ccdm.deploy.exceptions.CCDMException;

public interface IWebServerProviderTask 
{
	void doTask(SolutionHistory solutionHistory) throws CCDMException;
}
