package com.ccdm.deploy.webServer.provider.task.factory;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.instance.constants.EInstanceTask;
import com.ccdm.deploy.webServer.provider.task.ClientAgentInstallationTask;
import com.ccdm.deploy.webServer.provider.task.CreateInstanceTask;
import com.ccdm.deploy.webServer.provider.task.IWebServerProviderTask;
import com.ccdm.deploy.webServer.provider.task.MasterAgentConfigurationTask;
import com.ccdm.deploy.webServer.provider.task.ScriptExecutionTask;

@Component
public class WebServerProviderTaskFactory 
{
	@Autowired
	@Qualifier("createInstanceTask")
	private CreateInstanceTask createInstanceTask;
	
	@Autowired
	@Qualifier("masterAgentConfigurationTask")
	private MasterAgentConfigurationTask masterAgentConfigurationTask;
	
	@Autowired
	@Qualifier("clientAgentInstallationTask")
	private ClientAgentInstallationTask clientAgentInstallationTask;
	
	@Autowired
	@Qualifier("scriptExecutionTask")
	private ScriptExecutionTask scriptExecutionTask;
	
	private static final Map<String, IWebServerProviderTask> providerTaskMap = new HashMap<String, IWebServerProviderTask>();
	
	@PostConstruct
	private void initializeTaskMap()
	{
		providerTaskMap.put(EInstanceTask.CREATE_INSTANCE.getTaskName(), createInstanceTask);
		providerTaskMap.put(EInstanceTask.MASTER_AGENT_CONFIG.getTaskName(), masterAgentConfigurationTask);
		providerTaskMap.put(EInstanceTask.CLIENT_AGENT_INSTALL.getTaskName(), clientAgentInstallationTask);
		providerTaskMap.put(EInstanceTask.SCRIPT_EXECUTION.getTaskName(), scriptExecutionTask);
	}
	
	public IWebServerProviderTask getTask(String taskName) {
		return providerTaskMap.get(taskName);
	}
}
