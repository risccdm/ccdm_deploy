package com.ccdm.deploy.webServer.provider.task;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.agent.enums.ScripExecutionStatus;
import com.ccdm.deploy.agent.model.ExecutionScriptRequest;
import com.ccdm.deploy.agent.model.ExecutionScriptResponse;
import com.ccdm.deploy.elastic.solutionhistory.model.ScriptRequestVo;
import com.ccdm.deploy.elastic.solutionhistory.model.ScriptResponseVo;
import com.ccdm.deploy.elastic.vo.ScriptOutputLog;
import com.ccdm.deploy.elastic.vo.SolutionHistory;
import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.instance.constants.EInstanceTask;
import com.ccdm.deploy.script.controller.ScriptController;
import com.ccdm.deploy.solution.biz.ClassCastAdapter;
import com.ccdm.deploy.solution.constants.ESolutionState;
import com.ccdm.deploy.webServer.provider.task.utils.ProviderUtils;

@Component("scriptExecutionTask")
public class ScriptExecutionTask implements IWebServerProviderTask 
{
	private static Logger logger = LoggerFactory.getLogger(ScriptExecutionTask.class);
	
	@Autowired
	private ClassCastAdapter classCastAdapter;
	@Autowired
	private ScriptController scriptController;
	@Autowired
	private ProviderUtils taskUtils;
	
	@Override
	public void doTask(SolutionHistory solutionHistory) throws CCDMException
	{
		ScriptRequestVo scriptRequestVo = null;
		try
		{
			logger.info("Task-4 : '"+EInstanceTask.SCRIPT_EXECUTION+"'...");
			// step-0 : check Dependecy task has been finished or not by refId
			logger.info("Checking Dependecy Task for "+EInstanceTask.SCRIPT_EXECUTION.getTaskName());
			boolean isDependecyTaskFinished = taskUtils.isDependecyInstanceTaskCompleted(solutionHistory);
			
			if(isDependecyTaskFinished)
			{
				// step-1 Update INPROGRESS STATUS
				taskUtils.updateSolutionHistoryStatus(solutionHistory, ESolutionState.INPROGRESS.getSolutionState());
				logger.info("*************** "+ESolutionState.INPROGRESS.name()+" ***************");
				
				// step-2 call to Execute Script on Client Instance
				scriptRequestVo = (ScriptRequestVo) classCastAdapter.convertRequestToRealObject(solutionHistory, EInstanceTask.SCRIPT_EXECUTION.getTaskName(), ScriptRequestVo.class);
				logger.info("Executing Script on Instance Initiated...");
				ScriptResponseVo scriptResponseVo = executeScript(scriptRequestVo);
				
				// step-3 : saving response to solution history
				solutionHistory.setResponseObject(scriptResponseVo);
				taskUtils.saveOrUpdateSolutionHistory(solutionHistory);
				logger.info("response saved...!");
				
				// step-4 : saving Script output Log to ScriputOutputLog Collection & throw exception if error in script execution
				ScriptOutputLog scriptOutputLog = buildScriptOutputLog(scriptResponseVo);
				scriptOutputLog.setInstanceId(scriptRequestVo.getClientInstanceVo().getInstanceId());
				scriptOutputLog.setScriptId(scriptRequestVo.getScriptId());
				scriptController.saveScriptExectionLog(scriptOutputLog);
				logger.info("Script Log saved...!");
				if(scriptResponseVo.getStatus().equalsIgnoreCase(ScripExecutionStatus.FAILURE.getScriptLogStatus())) {
					throw new CCDMException(ErrorCodes.SCRIPT_EXECUTION__FAILED);
				}
				
				// step-5 : update COMPLETED STATUS
				taskUtils.updateSolutionHistoryStatus(solutionHistory, ESolutionState.COMPLETED.getSolutionState());
				logger.info("*************** "+ESolutionState.COMPLETED.name()+" ***************");
			}
			else 
			{
				taskUtils.updateSolutionHistoryStatus(solutionHistory, ESolutionState.SKIPPED.getSolutionState());
				logger.info(EInstanceTask.SCRIPT_EXECUTION.getTaskName()+" Task cannot be start. since dependecy task not Completed");
			}
		}
		catch(Exception  e) 
		{
			taskUtils.updateSolutionHistoryStatus(solutionHistory, ESolutionState.FAILED.getSolutionState());
			throw new CCDMException(ErrorCodes.SCRIPT_EXECUTION__FAILED);
		}
	}
	
	/* *********************************** Private Methods ********************************* */
	private ScriptOutputLog buildScriptOutputLog(ScriptResponseVo scriptResponseVo) 
	{
		ScriptOutputLog scriptOutputLog = new ScriptOutputLog();
		scriptOutputLog.setExecutionDate(scriptResponseVo.getExecutionDate());
		scriptOutputLog.setScriptOutput(scriptResponseVo.getLogMessage());
		scriptOutputLog.setStatus(scriptResponseVo.getStatus());
		return scriptOutputLog;
	}
	
	private ScriptResponseVo executeScript(ScriptRequestVo scriptRequestVo) throws InterruptedException, CCDMException 
	{
		ScriptResponseVo scriptResponseVo = null;
		try 
		{
			taskUtils.sleepThreadByTimeOut("Sleeping 5 sec to connect client instance...", 5000);
			
			// 1a. execute script on clientAgent
			ExecutionScriptRequest executionScriptRequest = new ExecutionScriptRequest();
			executionScriptRequest.setInstanceVo(scriptRequestVo.getClientInstanceVo());
			executionScriptRequest.setServerCredentialVo(scriptRequestVo.getServerCredentialVo());
			executionScriptRequest.setScript(scriptRequestVo.getScriptCommand());
			ExecutionScriptResponse response = scriptController.executeScriptOnInstance(executionScriptRequest);

			scriptResponseVo = new ScriptResponseVo();
			scriptResponseVo.setExecutionDate(response.getExecutedDate());
			scriptResponseVo.setLogMessage(response.getScriptOutput());
			scriptResponseVo.setStatus(ScripExecutionStatus.SUCCESS.getScriptLogStatus());
			return scriptResponseVo;
		} 
		catch(Exception e) 
		{
			scriptResponseVo = new ScriptResponseVo();
			scriptResponseVo.setExecutionDate(new Date());
			scriptResponseVo.setLogMessage(e.getMessage());
			scriptResponseVo.setStatus(ScripExecutionStatus.FAILURE.getScriptLogStatus());
			return scriptResponseVo;
		}
	}
}
