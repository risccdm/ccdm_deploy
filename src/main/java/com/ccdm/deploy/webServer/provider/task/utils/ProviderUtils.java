package com.ccdm.deploy.webServer.provider.task.utils;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.services.ec2.model.Instance;
import com.ccdm.deploy.elastic.repository.solutionhistory.ISolutionHistoryRepository;
import com.ccdm.deploy.elastic.solutionhistory.model.AgentClientRequestVo;
import com.ccdm.deploy.elastic.solutionhistory.model.AgentMasterConfigRequestVo;
import com.ccdm.deploy.elastic.solutionhistory.model.ScriptRequestVo;
import com.ccdm.deploy.elastic.vo.SolutionHistory;
import com.ccdm.deploy.errors.ErrorCodes;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.instance.constants.EInstanceTask;
import com.ccdm.deploy.instance.constants.InstanceConstant;
import com.ccdm.deploy.solution.biz.ClassCastAdapter;
import com.ccdm.deploy.solution.constants.ESolutionState;
import com.ccdm.deploy.webprovider.response.InstanceCreateResponse;

@Component
public class ProviderUtils 
{
private static Logger logger = LoggerFactory.getLogger(ProviderUtils.class);
	
	@Autowired
	private ISolutionHistoryRepository solutionHistoryRepo;
	@Autowired
	private ClassCastAdapter classCastAdapter;
	
	public boolean isDependecyInstanceTaskCompleted(SolutionHistory solutionHistory) 
	{
		SolutionHistory dependentSolutionHistory = solutionHistoryRepo.findOne(solutionHistory.getRefId());
		if(dependentSolutionHistory != null && dependentSolutionHistory.getStatus().equalsIgnoreCase(ESolutionState.COMPLETED.getSolutionState()))
		{
			return true;
		}
		return false;
	}
	
	public void updateSolutionHistoryStatus(SolutionHistory solutionHistory, String newStatus) 
	{
		solutionHistory.setStatus(newStatus);
		saveOrUpdateSolutionHistory(solutionHistory);
	}
	
	public void saveOrUpdateSolutionHistory(SolutionHistory solutionHistory)
	{
		solutionHistoryRepo.save(solutionHistory);
	}
	
	public void updateTicketSaltToClientAgent(SolutionHistory masterAgentSolutionHistory, String ticketSalt) throws CCDMException 
	{
		SolutionHistory clientAgentSolutionHistory = solutionHistoryRepo.findOneByInstanceTaskNameAndInstanceUniqueId(
														EInstanceTask.CLIENT_AGENT_INSTALL.getTaskName(), masterAgentSolutionHistory.getInstanceUniqueId());
		AgentClientRequestVo requestVo = (AgentClientRequestVo) classCastAdapter.convertRequestToRealObject(clientAgentSolutionHistory, clientAgentSolutionHistory.getInstanceTaskName(), AgentClientRequestVo.class);
		requestVo.setTicketSalt(ticketSalt);
		clientAgentSolutionHistory.setRequestObject(requestVo);
		saveOrUpdateSolutionHistory(clientAgentSolutionHistory);
	}
	
	public void sleepThreadByTimeOut(String sleepMsg, long timeout) throws CCDMException
	{
		try 
		{
			logger.info(sleepMsg);
			Thread.sleep(timeout);
		}
		catch(Exception e) {
			throw new CCDMException(ErrorCodes.THREAD_SLEEP_FAILED);
		}
	}
	
	public List<String> getInstanceIds(List<Instance> instances) 
	{
		List<String> instanceIds = new ArrayList<String>();
		for(Instance instance : instances)
		{
			instanceIds.add(instance.getInstanceId());
		}
		return instanceIds;
	}
	
	public List<String> getSecurityGroups() 
	{
		List<String> securityGroups = new ArrayList<String>();
		securityGroups.add(InstanceConstant.SECURITY_GROUPS);
		return securityGroups;
	}
	
	public void updatedCreatedInstanceDetails(SolutionHistory newSolutionHistory) throws CCDMException 
	{
		List<SolutionHistory> solutionHistoryList = solutionHistoryRepo.findAllByInstanceUniqueId(newSolutionHistory.getInstanceUniqueId());
		InstanceCreateResponse instanceCreateResponse = (InstanceCreateResponse) classCastAdapter.convertResponseToRealObject(newSolutionHistory, newSolutionHistory.getInstanceTaskName(), InstanceCreateResponse.class);
		
		if(solutionHistoryList != null && solutionHistoryList.size() > 0)
		{
			for(SolutionHistory solution : solutionHistoryList)
			{
				// setting common values
				solution.setInstanceName(newSolutionHistory.getInstanceName());
				
				if(solution.getInstanceTaskName().equalsIgnoreCase(EInstanceTask.MASTER_AGENT_CONFIG.getTaskName()))
				{
					AgentMasterConfigRequestVo requestVo = (AgentMasterConfigRequestVo) classCastAdapter.convertRequestToRealObject(solution, solution.getInstanceTaskName(), AgentMasterConfigRequestVo.class);
					requestVo.setClientInstanceVo(instanceCreateResponse.getClientInstanceVo());
					solution.setRequestObject(requestVo);
				}
				if(solution.getInstanceTaskName().equalsIgnoreCase(EInstanceTask.CLIENT_AGENT_INSTALL.getTaskName()))
				{
					AgentClientRequestVo requestVo = (AgentClientRequestVo) classCastAdapter.convertRequestToRealObject(solution, solution.getInstanceTaskName(), AgentClientRequestVo.class);
					requestVo.setClientInstanceVo(instanceCreateResponse.getClientInstanceVo());
					solution.setRequestObject(requestVo);
				}
				if(solution.getInstanceTaskName().equalsIgnoreCase(EInstanceTask.SCRIPT_EXECUTION.getTaskName()))
				{
					ScriptRequestVo requestVo = (ScriptRequestVo) classCastAdapter.convertRequestToRealObject(solution, solution.getInstanceTaskName(), ScriptRequestVo.class);
					requestVo.setClientInstanceVo(instanceCreateResponse.getClientInstanceVo());
					solution.setRequestObject(requestVo);
				}
				solutionHistoryRepo.save(solution);
			}
		}
	}
}
