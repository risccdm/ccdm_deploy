package com.ccdm.deploy.webServer.provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.elastic.repository.solutionhistory.ISolutionHistoryRepository;
import com.ccdm.deploy.elastic.vo.SolutionHistory;
import com.ccdm.deploy.webServer.provider.task.factory.WebServerProviderTaskFactory;

@Component("webServerProviderProcessor")
public class ProviderTaskExecutor implements IWebServerProviderService
{
	private static Logger logger = LoggerFactory.getLogger(ProviderTaskExecutor.class);
	
	@Autowired
	private ISolutionHistoryRepository solutionHistoryRepo;
	@Autowired
	private WebServerProviderTaskFactory webServerProviderTaskFactory;
	
	@Override
	public void executeTaskByProvider(String solutionHistoryId) 
	{
		SolutionHistory solutionHistory = solutionHistoryRepo.findOne(solutionHistoryId);
		try
		{
			if(solutionHistory != null)
			{
				webServerProviderTaskFactory.getTask(solutionHistory.getInstanceTaskName()).doTask(solutionHistory);
			}
		}
		catch(Exception e)
		{
			logger.info("TASK "+solutionHistory.getInstanceTaskName()+ " is Failed.!");
			logger.error(e.getMessage());
		}
	}
}
