package com.ccdm.deploy.webServer.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.ccdm.deploy.provider.constants.EProvider;
import com.ccdm.deploy.webprovider.service.AWSProviderServiceImpl;
import com.ccdm.deploy.webprovider.service.GCEProviderServiceImpl;
import com.ccdm.deploy.webprovider.service.IProviderService;

@Component
public class WebServerProviderFactory 
{
	@Autowired
	@Qualifier("awsProvider")
	private AWSProviderServiceImpl awsProviderService;
	@Autowired
	@Qualifier("gceProvider")
	private GCEProviderServiceImpl gceProviderService;
	
	public IProviderService getWebProviderByProviderName(String providerName)
	{
		if(providerName.equalsIgnoreCase(EProvider.AMAZON.getProviderName()))
		{
			return awsProviderService;
		}
		else if(providerName.equalsIgnoreCase(EProvider.GOOGLE.getProviderName()))
		{
			return gceProviderService;
		}
		return null;
	}
}
