package com.ccdm.deploy.property;

public class PropertyConstants 
{
	public static final String DEPLOY_APP_MYSQL_IP 		= "[DEPLOY_APP_MYSQL_IP]";
	public static final String DEPLOY_APP_MYSQL_PORT 	= "[DEPLOY_APP_MYSQL_PORT]";
	
	//MySql properties Name
	public static final String MYSQL_DATASOURCE_URL = "deployApp.mysql.DataSource.URL";
	public static final String MYSQL_DATABASE_NAME  = "deployApp.mysql.databaseName";
	public static final String MYSQL_DRIVER_NAME  	= "deployApp.mysqlDriver";
	public static final String MYSQL_IP_ADDRESS 	= "deployApp.mysql.ip";
	public static final String MYSQL_PORT_NUMBER 	= "deployApp.mysql.port";
	public static final String MYSQL_USER_NAME 		= "deployApp.mysql.username";
	public static final String MYSQL_PASSWORD 		= "deployApp.mysql.password";
	
	//Domain Configuration
	public static final String DEPLOY_APP_DOMAIN_NAME  	= "deployApp.domain";
	
	// Domain Auth Configuration
	public static final String DEPLOY_APP_XAUTH_TOKEN  	= "deployApp.xAuthToken";
	
	public static final String ELASTIC_CLIENT_TRANSPORT_PING_TIME_OUT   		= "client.transport.ping_timeout";
	public static final String ELASTIC_CLIENT_TRANSPORT_SNIFF           		= "client.transport.sniff";
	public static final String ELASTIC_CLIENT_TRANSPORT_NODES_SAMPLER_INTERVAL  = "client.transport.nodes_sampler_interval";
	
	public static final String ELASTIC_CLIENT_TRANSPORT_IP   	= "client.transport.ip";
	public static final String ELASTIC_CLIENT_TRANSPORT_PORT   	= "client.transport.port";
	
	public static final String ELASTIC_CLUSTER_NAME				= "cluster.name";
	public static final String ELASTIC_CLUSTER_TYPE_SOLUTION_HISTORY = "cluster.type.solutionhistory";
	
	public static final String CORE_POOL_SIZE = "corePoolSize";
	public static final String MAX_POOL_SIZE = "maxPoolSize";
	public static final String THREAD_NAME_PREFIX = "threadNamePrefix";
	public static final String WAIT_FOR_TASK_TO_COMPLETE_ON_SHUTDOWN = "waitForTasksToCompleteOnShutdown";
}
