package com.ccdm.deploy.script.service;

import com.ccdm.deploy.elastic.vo.ScriptOutputLog;

public interface IScriptService 
{
	void saveLogMessage(ScriptOutputLog scriptOutputLog);
}
