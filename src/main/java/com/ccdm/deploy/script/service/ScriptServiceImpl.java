package com.ccdm.deploy.script.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ccdm.deploy.elastic.repository.scriptlog.IScriptOutputLogRepository;
import com.ccdm.deploy.elastic.vo.ScriptOutputLog;

@Repository
@Transactional
public class ScriptServiceImpl implements IScriptService 
{
	@Autowired
	private IScriptOutputLogRepository scriptOutputLogRepository;

	public void saveLogMessage(ScriptOutputLog log) 
	{
		scriptOutputLogRepository.save(log);
	}
}
