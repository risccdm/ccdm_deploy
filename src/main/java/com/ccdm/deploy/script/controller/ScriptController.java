package com.ccdm.deploy.script.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.ccdm.deploy.agent.model.ExecutionScriptRequest;
import com.ccdm.deploy.agent.model.ExecutionScriptResponse;
import com.ccdm.deploy.agent.service.AgentService;
import com.ccdm.deploy.base.controller.BaseController;
import com.ccdm.deploy.elastic.vo.ScriptOutputLog;
import com.ccdm.deploy.exceptions.CCDMException;
import com.ccdm.deploy.script.service.IScriptService;

@RestController
public class ScriptController extends BaseController 
{
	@Autowired
	private AgentService agentService;

	@Override
	protected Class<?> getClassName() {
		return this.getClass();
	}

	@Autowired
	private IScriptService scriptService;
	
	public ExecutionScriptResponse executeScriptOnInstance(ExecutionScriptRequest executionScriptRequest) throws CCDMException
	{
		ExecutionScriptResponse scriptResponse = agentService.executeScript(executionScriptRequest);
		return scriptResponse;
	}
	
	public void saveScriptExectionLog(ScriptOutputLog scriptOutputLog)
	{
		scriptService.saveLogMessage(scriptOutputLog);
	}
}
